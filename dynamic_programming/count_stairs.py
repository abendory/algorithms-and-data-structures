def stairs(n):
    if n in (1,2): return n
    first, second = 1, 2

    for n in range(3, n):
        first, second = second, first + second

    return first + second


print(stairs(2))
