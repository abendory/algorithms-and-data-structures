#!/usr/bin/env python3

# S{O[MN]}
def best_path_dp(grid):
    m = len(grid[0])  # num of cols
    n = len(grid)     # num of rows

    cache = [[None for y in range(m)] for x in range(n)]
    prev = 0

    for i in range(m-1, -1, -1):
        prev = prev + grid[n-1][i]
        cache[n-1][i] = prev

    for i in range(n-2, -1, -1):
        cache[i][m-1] = cache[i+1][m-1] + grid[i][m-1]
        for j in range(m-2, -1, -1):
            cache[i][j] = (grid[i][j] + (cache[i][j+1] if cache[i][j+1] > cache[i+1][j]
                else cache[i+1][j]))

    return cache[0][0]

# you can reverse the path to go from bottom left to top right to make the loops more intuitive (i.e., 0 to N-2 and 0 to M-2)
def best_path_dp2(grid):
    m = len(grid[0])  # num of cols
    n = len(grid)     # num of rows

    cache = [[None for y in range(m)] for x in range(n)]
    prev = 0

    for i in range(m-1, -1, -1):
        prev = prev + grid[n-1][i]
        cache[n-1][i] = prev

    for i in range(n-2, -1, -1):
        cache[i][m-1] = cache[i+1][m-1] + grid[i][m-1]
        for j in range(m-2, -1, -1):
            cache[i][j] = (grid[i][j] + (cache[i][j+1] if cache[i][j+1] > cache[i+1][j]
                else cache[i+1][j]))

    return cache[0][0]

# you can save space
def best_path_dp3(grid):
    # implement only saving each new row and prev row in memory
    pass


# you can save space
def best_path_dp2(grid):
    m = len(grid[0])  # num of cols
    n = len(grid)     # num of rows

    cache = [[None for y in range(m)] for x in range(n)]
    prev = 0

    for i in range(m-1, -1, -1):
        prev = prev + grid[n-1][i]
        cache[n-1][i] = prev

    for i in range(n-2, -1, -1):
        cache[i][m-1] = cache[i+1][m-1] + grid[i][m-1]
        for j in range(m-2, -1, -1):
            cache[i][j] = (grid[i][j] + (cache[i][j+1] if cache[i][j+1] > cache[i+1][j]
                else cache[i+1][j]))

    return cache[0][0]

print(best_path([[1,3,5,1],[3,6,3,1],[2,5,6,1]]))
