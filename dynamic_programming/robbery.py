"""
Robbery
There are n houses built in a line, each of which contains some value in it. A thief is going to steal the maximal value in these houses, but he cannot steal in two adjacent houses because the owner of a stolen house will tell his two neighbors on the left and right side. What is the maximal stolen value?

For example, if there are four houses with values {6, 1, 2, 7}, the maximal stolen value is 13 when the first and fourth houses are stolen.
"""

def robbery_r(p, i, j):
    if i == j: return 0
    return max(p[i] + robbery_r(p, i+2, j), robbery_r(p, i+1, j))

def robbery_m(p, i, j):
    cache = [0 for i in range(len(p)+1)]
    return robbery_m_helper(p, i, j, cache)

def robbery_m_helper(p, i, j, m):
    if i == j: return 0
    if m[i]: return m[i]
    m[i] = max(p[i] + robbery_m_helper(p, i+2, j, m), robbery_m_helper(p, i+1, j, m))
    return m[i]


def robbery_d(p):

    v1 = p[0]
    v2 = max(p[0], p[1])

    for i in range(2, len(p)):
        v3 = max(v2, v1+p[i])
        v1 = v2
        v2 = v3

    return v3
