
# How many numbers can I construct using 10 digits starting from 1?
# Answer: Num of 9 dig nums starting from 8 + num of 9 dig nums starting from 6

# How many numbers can I construct using 9 digits starting from 8?
# Answer: Num of 8 dig nums starting from 1 + num of 9 dig nums starting from 3

# How many numbers can I construct using 9 digits starting from 6?
# Answer: Num of 8 dig nums starting from 1 + num of 9 dig nums starting from 7

# ...

# How many numbers can I construct using 1 digits starting from X?

# keep track using 2d array, num_digits[digit][starting_from]
# so num_digits[1][i] = 1 where 0 <= i <= 9


def traverse_dp(digits, start):
    key_map = {
          0: [4, 6],
          1: [6, 8],
          2: [7, 9],
          3: [4, 8],
          4: [3, 9, 0],
          5: [],
          6: [1, 7, 0],
          7: [2, 6],
          8: [1, 3],
          9: [2, 4]
    }

    # initialize NxN array to 0
    num_digits = [[None for i in range(10)] for j in range(digits+1)]

    # populate base case
    for i in range(10):
        num_digits[0][i] = 0
        num_digits[1][i] = 1

    for digit in range(2, digits+1):
        for i in range(10):
            # use a generator expression
            num_digits[digit][i] = sum(num_digits[digit - 1][x] for x in key_map[i])


    return num_digits[digits][start]

digits = 10
start = 1
print(traverse_dp(digits, start))
