from pprint import pprint as pp

# constants
INF = float('inf')
INT_MAX = 2**31-1
INT_MIN = -2**31+1

class DynamicProgramming:

    """
     #1 longest common subsequence
       return the longest common subsequence between two strings
    """
    def longest_common_subsequence(self, x, y):
        m = len(x)
        n = len(y)

        c = [['' for j in range(n+1)] for i in range(m+1)]
        for i in range(1, m+1):
            for j in range(1, n+1):
                if x[i-1] == y[j-1]:
                    c[i][j] = c[i-1][j-1] + x[i-1]
                else:
                    c[i][j] = max(c[i-1][j], c[i][j-1], key=len)

        print(c[m][n])
        return c[m][n]

    #2 length longest increasing subsequence
    def length_longest_increasing_subsequence(self, x, y):
        pass

    #3 making change
    def making_change(self, change, coins):
        pass

    #4 edit distance
    def edit_distance(self, x, y):
        pass

    """
     #5 max size submatrix - print the largest submatrix within a given matrix
    """
    def max_size_submatrix(self, m):
        rows, cols = len(m), len(m[0])
        c = [[0 for j in range(cols)] for i in range(rows)]

        for i in range(rows):
            c[i][0] = m[i][0]

        for j in range(cols):
            c[0][j] = m[0][j]

        for i in range(1, rows):
            for j in range(1, cols):
                if m[i][j] == 0:
                    c[i][j] = 0
                else:
                    c[i][j] = min(c[i-1][j], c[i][j-1], c[i-1][j-1]) + 1

        max_index = 0
        for i in range(rows):
            for j in range(cols):
                if c[i][j] > max_index:
                    max_index = c[i][j]

        # print the submatrix with length max_index
        for j in range(max_index):
            for i in range(max_index):
                print(1, end=' ')
            print()


    """
     #6 knight's tour - how many 10 digit numbers (numbers) can you get starting from 1 (start)
      keypad: 1 2 3
              4 5 6
              7 8 9
                0
    """
    def knights_tour(self, numbers, start):
        keymap = {0:{4,6}, 1:{6,8}, 2:{7,9}, 3:{4,8}, 4:{0,3,9}, 5:{}, 6:{1,7,0}, 7:{2,6}, 8:{1,3}, 9:{2,4}}

        num_digits_from = [[0 for j in range(10)] for i in range(numbers+1)]

        # number of zero digit numbers starting from i
        # number of one digit numbers starting from i
        for i in range(10):
            num_digits_from[0][i] = 0
            num_digits_from[1][i] = 1

        for i in range(2, numbers+1):
            for j in range(10):
                # number of 2 dig nums from j = sum of number of 1 dig nums from j's mapped values
                num_digits_from[i][j] = sum([num_digits_from[i-1][x] for x in keymap[j]])

        print(num_digits_from[numbers][start])
        return num_digits_from[numbers][start]

    """
     #7 matrix chain multiplication
      Given a sequence of matrices, find the most efficient way to multiply these matrices
      together. The problem is not actually to perform the multiplications, but merely to
      decide in which order to perform the multiplications.
    """
    def matrix_chain_multiplication(self, p):
        n = len(p)
        c = [[0 for i in range(n)] for i in range(n)]

        for L in range(2, n):
            for i in range(1, n-L+1):
                j = i+L-1
                c[i][j] = INF
                for k in range(i, j):
                    cost = c[i][k] + c[k+1][j] + p[i-1]*p[j]*p[k]
                    c[i][j] = min(cost, c[i][j])

        print(c[1][n-1])
        return c[1][n-1]



""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

# Test
dp = DynamicProgramming()
# dp.longest_common_subsequence('thisisatest', 'testinglcs123testing')
# dp.max_size_submatrix([[0, 1, 1, 0, 1], [1, 1, 0, 1, 0], [0, 1, 1, 1, 0], [1, 1, 1, 1, 0], [1, 1, 1, 1, 1], [0, 0, 0, 0, 0]])
# dp.knights_tour(10, 1)  # number of 10 digit numbers starting from 1
# dp.matrix_chain_multiplication([4, 2, 3, 8, 3])




    
