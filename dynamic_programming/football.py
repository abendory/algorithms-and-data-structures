"""
In a football game you can score a safety '2', TD '7', or field goal '3'
Show all combinations that yield a given score
"""
# Assuming score of 12
# 12 = score 10 + SY, 5 + TD, or 9 + FG


def football(score, combinations=None):
    if score < 0:
        return

    if combinations == None:
        combinations = []

    if score == 0:
        return combinations

    FIELDGOAL = 3
    SAFETY = 2
    TOUCHDOWN = 7

    football(score - FIELDGOAL, combinations)
    football(score - TOUCHDOWN, combinations)
    football(score - SAFETY, combinations)

print(football(12))
