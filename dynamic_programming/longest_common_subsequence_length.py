# Recursive Algorithm
# if x[i] == y[j]: 1 + lcs(i+1, j+1)
# if x[i] != y[j]: lcs(i, j+1)
# if x[i] != y[j]: lcs(i+1, j)

def lcs_r(x, y):
    if not x or not y: return ''
    if x[0] == y[0]: return x[0] + lcs_r(x[1:], y[1:])
    return max(lcs_r(x, y[1:]), lcs_r(x[1:], y), key=len)

def lcs_recursive(x, y):
    def lcs_recursive(x, y, i, j):
        m = len(x)
        n = len(y)
        if i == m or j == n:
            return ''

        if x[i] == y[j]:
            return x[i] + lcs_recursive(x, y, i+1, j+1)

        return max(lcs_recursive(x, y, i+1, j), lcs_recursive(x, y, i, j+1), key=len)
    return lcs_recursive(x, y, 0, 0)

def lcs_memoized(x, y):
    def _lcs_memoized(x, y, i, j, cache):
        m, n = len(x), len(y)

        if i == m or j == n:
            return ''

        if cache[i][j]:
            return cache[i][j]

        if x[i] == y[j]:
            cache[i][j] = x[i] + _lcs_memoized(x, y, i+1, j+1, cache)
        else:
            cache[i][j] = max(_lcs_memoized(x, y, i+1, j, cache),
                            _lcs_memoized(x, y, i, j+1, cache), key=len)

        return cache[i][j]

    m, n = len(x), len(y)
    cache = [['' for j in range(n+1)] for i in range(m+1)]
    return _lcs_memoized(x, y, 0, 0, cache)

def lcs_dynamic(x, y):
    m = len(x)
    n = len(y)

    table = [['' for j in range(n+1)] for i in range(m+1)]
    for i in range(1, m+1):
        for j in range(1, n+1):
            if x[i-1] == y[j-1]:
                table[i][j] = table[i-1][j-1] + x[i-1]
            else:
                table[i][j] = max(table[i-1][j], table[i][j-1], key=len)
    return table[m][n]

print(lcs_recursive('thisisatest', 'testinglcs123testing')) # tsistest
print(lcs_memoized('thisisatest', 'testinglcs123testing'))
print(lcs_dynamic('thisisatest', 'testinglcs123testing'))
