def is_interleaving_r(str1, str2, endstr):
    if len(str1) + len(str2) != len(endstr):
        return False

    if len(str1) == 0 and len(str2) == 0:
        return True

    # find str1 char in endstr
    res = (str1 and str1[0] == endstr[0] and is_interleaving_r(str1[1:], str2, endstr[1:]) or
           str2 and str2[0] == endstr[0] and is_interleaving_r(str1, str2[1:], endstr[1:]))

    return res if res else False

def is_interleaving_r_indices(str1, str2, endstr, i, j, k):
    if len(str1) + len(str2) != len(endstr):
        return False

    if i == len(str1) and j == len(str2):
        return True

    # find str1 char in endstr
    return (i < len(str1) and str1[i] == endstr[k] and
                is_interleaving_r_indices(str1, str2, endstr, i+1, j, k+1) or
            j < len(str2) and str2[j] == endstr[k] and
                is_interleaving_r_indices(str1, str2, endstr, i, j+1, k+1))

def is_interleaving_dp(str1, str2, endstr):
    table = [[False for j in range(len(str2)+1)] for i in range(len(str1)+1)]

    if len(str1) + len(str2) != len(endstr):
        return False

    for i in range(len(str1)):
        for j in range(len(str2)):
            length = i + j - 1
            if i == 0 and j == 0:
                table[i][j] = True

            elif i == 0 and str2[j-1] == endstr[j-1]:
                table[i][j] = table[i][j-1]

            elif j == 0 and str1[i-1] == endstr[i-1]:
                table[i][j] = table[i-1][j]

            elif str1[i-1] == endstr[i+j-1] and str2[j-1] != endstr[i+j-1]:
                table[i][j] = table[i-1][j]

            elif str2[j-1] == endstr[i+j-1] and str1[i-1] != endstr[i+j-1]:
                table[i][j] = table[i][j-1]

            elif str1[i-1] == endstr[i+j-1] and str2[j-1] == endstr[i+j-1]:
                table[i][j] = table[i-1][j] or table[i][j-1]

    return table[len(str1)][len(str2)]

print(is_interleaving_r("XXY", "XXZ", "XXZXXXY"))
print(is_interleaving_r("XY" ,"WZ" ,"WZXY"))
print(is_interleaving_r("XY", "X", "XXY"))
print(is_interleaving_r("YX", "X", "XXY"))
print(is_interleaving_r("XXY", "XXZ", "XXXXZY"))
print()
print(is_interleaving_r_indices("XXY", "XXZ", "XXZXXXY", 0, 0, 0))
print(is_interleaving_r_indices("XY" ,"WZ" ,"WZXY", 0, 0, 0))
print(is_interleaving_r_indices("XY", "X", "XXY", 0, 0, 0))
print(is_interleaving_r_indices("YX", "X", "XXY", 0, 0, 0))
print(is_interleaving_r_indices("XXY", "XXZ", "XXXXZY", 0, 0, 0))
print()
print(is_interleaving_dp("XXY", "XXZ", "XXZXXXY"))
print(is_interleaving_dp("XY" ,"WZ" ,"WZXY"))
print(is_interleaving_dp("XY", "X", "XXY"))
print(is_interleaving_dp("YX", "X", "XXY"))
print(is_interleaving_dp("XXY", "XXZ", "XXXXZY"))
