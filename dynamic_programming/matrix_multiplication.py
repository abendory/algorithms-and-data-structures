# we're going to use a catalan number sequence
# loop through and recurse each side
# base case is 0
# example [a, b, c, d, e]
# f([a]) * f([b, c, d, e])
# f([a, b]) * f([c, d, e])
# f([a, b, c]) * f([d, e]) etc...

def matrixmult_r(m, i, j):
    if i == j:
        return 0

    # init the min value to the max system value
    min_cost = float('inf')

    for index in range(i, j):
        cost = (matrixmult_r(m, i, index) +
                 matrixmult_r(m, index + 1, j) +
                 m[i-1] * m[index] * m[j])

        min_cost = min(cost, min_cost)

    return min_cost


# we're going to use a catalan number sequence
# loop through and segment
# example [a, b, c, d, e]
# f([a]) + f([b, c, d, e])
# f([a, b]) + f([c, d, e])
# f([a, b, c]) + f([d, e]) etc...
# cache is 2d list
"""
Matrix-Chain(array p[1..n]) {
    array s[1..n-1,2..n]
    for i = 1 to n:
        m[i,i] = 0 // initialize
    for L = 2 to n: // L = length of subchain
        for i = 1 to n-L+1:
            j = i + L - 1
            m[i,j] = float('inf')
            for k = i to j-1  // check all splits
                q = m[i, k] + m[k+1, j] + p[i-1]*p[k]*p[j]
                if (q < m[i, j])
                    m[i,j] = q
                    s[i,j] = k

    return m[1,n] (final cost) and s (splitting markers);
"""


def matrixmult_dp(p):
    n = len(p)
    m = [[0 for x in range(n)] for x in range(n)]

    for L in range(2, n):
        print('L =',L)
        for i in range(1, n - L + 1):
            j = i + L - 1
            print('i j =', i, j)
            m[i][j] = float('inf')
            for k in range(i, j):
                print('k =', k)
                cost = m[i][k] + m[k+1][j] + p[i-1]*p[k]*p[j]
                if cost < m[i][j]:
                    m[i][j] = cost

    return m[1][n-1]


arr = [2, 3, 5, 6, 4]
#print(matrixmult_r(arr, 1, len(arr)-1))
matrixmult_dp(arr)



