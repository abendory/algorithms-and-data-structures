def msum(a):
    return max((sum(a[j:i]), (j,i)) for i in range(1,len(a)+1) for j in range(i))

print(msum([1,5,-3,4,-2,1]))


def msum2(a):
    bounds, max_max, curr_max, start = (0,0), -float('inf'), 0, 0

    for end in range(len(a)):
        curr_max += a[end]
        if curr_max > max_max: bounds, max_max = (start, end+1), curr_max
        if curr_max < 0: curr_max, start = 0, end+1
    return (max_max, bounds)

print(msum2([1,5,-3,4,-2,1]))

