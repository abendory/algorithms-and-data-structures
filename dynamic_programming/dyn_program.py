
class DynamicProgramming:

    def balanced_partition(self, array):
        if len(array)<2 or len(array)%2>0 or sum(array)%2>0:
            return False
        # Normalize the input array
        min_value=min(array)
        array=[v-min_value for i, v in enumerate(array)]
        array_sum=sum(array);
        M=[[0 for amt in range(len(array)+1)] for element in range(array_sum+1)]
        M[0][0]=1; 
        for col in range(1, len(array)+1):
            for row in range(array_sum+1):
                if M[row][col-1]==1:
                    M[row][col]=1
                    M[row+array[col-1]][col]=1
                    if M[array_sum//2][col]==1:
                        return True
        return M[array_sum//2][col]==1

    def coin_play(self, coins):
        M=[[0 for j in range(len(coins))] for i in range(len(coins))]
        for k in range(len(coins)):
            i=0
            for j in range(k, len(coins)):
                if i==j:
                    M[i][j]=coins[i]
                elif i+1==j:
                    M[i][j]=max(coins[i], coins[j])
                else:
                    iplus1jminus1=M[i+1][j-1] if (i+1<len(coins) and j>0) else 0
                    iplus2j=M[i+2][j] if (i+2<len(coins)) else 0
                    ijminus2=M[i][j-2] if (j-2>=0) else 0
                    M[i][j]=max(coins[i]+min(iplus2j, iplus1jminus1), coins[j]+min(iplus1jminus1, ijminus2))
                i+=1
        print(M[0][len(coins)-1])

    def levenshtein_distance(self, str1, str2):
        if not str1 or not str2:
            return None
        if len(str1)==0:
            return len(str2)
        if len(str2)==0:
            return len(str1)
        M=[[0 for row in range(len(str2)+1)] for col in range(len(str1)+1)]; 
        for row in range(len(str1)+1):
            for col in range(len(str2)+1):
                if row==0:
                    M[row][col]=col
                if col==0:
                    M[row][col]=row
                if str1[row-1]==str2[col-1]:
                    M[row][col]=M[row-1][col-1]
                else:
                    M[row][col]=1+min(M[row-1][col], M[row][col-1], M[row-1][col-1])
        print(M[len(str1)][len(str2)])

    def longest_common_subsequence(self, str1, str2):
        M=[["" for col in range(len(str2)+1)] for j in range(len(str1)+1)]
        for row in range(len(str1)-1, -1, -1):
            for col in range(len(str2)-1, -1, -1):
                if str1[row]==str2[col]:
                    M[row][col]=str1[row]+M[row+1][col+1]
                else:
                    M[row][col]=M[row+1][col] if (len(M[row+1][col])>=len(M[row][col+1])) else M[row][col+1]
        print(M[0][0])

    def making_change(self, coins, change):
        M=[0 for i in range(change+1)]
        M[0]=1
        for i, coin in enumerate(coins):
            for amount in range(coin, change+1):
                M[amount] += M[amount-coin]
        print("%s ways to make change for %s" % (M[change], change))

    def strings_interleave(self, str1, str2, intrlvd):
        if len(str1)+len(str2)!=len(intrlvd):
            return False;
        if len(str1)==0:
            return len(str2)!=len(intrlvd)
        if len(str2)==0:
            return len(str1)!=len(intrlvd)
        M=[[0 for col in range(len(str2)+1)] for row in range(len(str1)+1)]
        M[0][0]=1
        for row in range(1, len(str1)+1):
            if str1[row-1]==intrlvd[row-1]:
                M[row][0]=M[row-1][0]
        for col in range(1, len(str2)+1):
            if str2[col-1]==intrlvd[col-1]:
                M[0][col]=M[0][col-1]
        for row in range(1, len(str1)+1):
            for col in range(1, len(str2)+1):
                if str1[row-1]==intrlvd[row+col-1] and M[row-1][col]==1 or str2[col-1]==intrlvd[row+col-1] and M[row][col-1]==1:
                    M[row][col]=1
        print("{} is".format(intrlvd), end=" ")
        if M[len(str1)][len(str2)]!=1:
            print("not", end=" ")
        print("interleaved")


dp = DynamicProgramming()

dp.levenshtein_distance("kitten", "sitting")
dp.longest_common_subsequence("AGGTAB", "GXTXAYB")
dp.making_change([2,3,5,6], 10)
print(dp.balanced_partition([4, 1, -5, 6, -11, 3]))
print(dp.strings_interleave("abd", "bac", "abacbd"))
dp.coin_play([8, 15, 3, 7])
