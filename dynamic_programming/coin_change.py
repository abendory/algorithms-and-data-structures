# naive, for loop around recursive call (many for loops in theory)
def change_dp(denoms, change):
    min_coins = [0]*(change+1)
    coins_used = [0]*(change+1)
    for cents in range(change+1):
        coin_count = cents
        new_coin = 1
        for j in [c for c in denoms if c <= cents]:
            if min_coins[cents-j] + 1 < coin_count:
                coin_count = min_coins[cents-j]+1
                new_coin = j
        min_coins[cents] = coin_count
        coins_used[cents] = new_coin

    coin = change
    coins = []
    while coin:
        coins.append(coins_used[coin])
        coin -= coins_used[coin]

    return coins


change = 101
denominations = [1,5,10,25]
coins = change_dp(denominations, change)
print(coins)

# to cache this we realise that only the 'change' variable changes so we store every value of 'change' in the cache
# using a for loop we build an array from 0 to change and for each value we store the number of coins we need
# a[0] = 0, a[1] = 1,  a[2] = 2,  a[3] = 3,  a[4] = 4,  a[5] = 1,  a[6] = 2,  a[7] = 3,  a[8] = 4,
# a[9] = 5, a[10] = 1, a[11] = 2, a[12] = 3, a[13] = 4, a[15] = 2, a[16] = 3, a[17] = 3, a[18] = 4
# to save space we only need the values for the change minus each denomination
# so for change = 75 we only need to store f(74), f(70), f(65), f(50)
# for really large values you can use a dictionary as a cache rather than an array but this only works during recursion, not for loop

