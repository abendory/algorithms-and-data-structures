#!/usr/bin/env python

def largest_submatrix(matrix):
    rows = len(matrix)
    cols = len(matrix[0])

    # construct matrix2
    cache = [[0 for j in range(cols)] for i in range(rows)]

    # set first row and first col
    for i in range(rows):
        cache[i][0] = matrix[i][0]
    for j in range(cols):
        cache[0][j] = matrix[0][j]

    # set remaining rows and cols
    for i in range(1, rows):
        for j in range(1, cols):
            if matrix[i][j] == 0:
                cache[i][j] = 0
            else: # matrix[i][j] = 1:
                cache[i][j] = min(cache[i-1][j], cache[i][j-1], cache[i-1][j-1]) + 1

    # get max
    maxi = 0
    for i in range(rows):
        for j in range(cols):
            if cache[i][j] > maxi:
                maxi = cache[i][j]

    # print matrix
    for j in range(maxi):
        for i in range(maxi):
            print('1', end=' ')
        print()


matrix = [[0, 1, 1, 0, 1],
          [1, 1, 0, 1, 0],
          [0, 1, 1, 1, 0],
          [1, 1, 1, 1, 0],
          [1, 1, 1, 1, 1],
          [0, 0, 0, 0, 0]]

largest_submatrix(matrix)
