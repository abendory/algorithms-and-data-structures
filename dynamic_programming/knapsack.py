"""
 Knapsack Problem
  You are an art thief who has found a way to break into the impressionist wing at the Art Institute of Chicago. Obviously you can't take everything. In particular, you're constrained to take only what your knapsack can hold — let's say it can only hold W pounds. You also know the market value for each painting. Given that you can only carry W pounds what paintings should you steal in order to maximize your profit?

 Solution
    Say there are n paintings with weights w1, ..., wn and market values v1, ..., vn. Define A(i,j) as the maximum value that can be attained from considering only the first i items weighing at most j pounds as follows.

    Let A[i][j] be the result where i is the value and j in the weight

    Obviously A(0,j) = 0 and A(i,0) = 0 for any i ≤ n and j ≤ W. If wi > j then A(i,j) = A(i-1, j) since we cannot include the ith item. If, however, wi ≤ j then A(i,j) then we have a choice: include the ith item or not. If we do not include it then the value will be A(i-1, j). If we do include it, however, the value will be vi + A(i-1, j - wi). Which choice should we make? Well, whichever is larger, i.e., the maximum of the two.

            { 0                                  if i == 0 or j == 0 }
  A[i][j] = { A[i-1][j]                          if Wi > j           }
            { max(A[i-1][j], Vi + A[i-1][j-Wi])  if Wi < j           }
"""


def knapsack_r(v, w, i, j):
    if i == 0 or j == 0: return 0
    if w[i-1] > j: return knapsack_r(v, w, i-1, j)
    if w[i-1] <= j: return max(knapsack_r(v, w, i-1, j), v[i-1] + knapsack_r(v, w, i-1, j-w[i-1]))

def knapsack_m(v, w, i, j):
    def knapsack_m(v, w, i, j, m):
        if i == 0 or j == 0: return 0
        if m[i][j]: return m[i][j]
        if w[i-1] > j: m[i][j] = knapsack_m(v, w, i-1, j, m)
        elif w[i-1] <= j: m[i][j] = max(knapsack_m(v, w, i-1, j, m), v[i-1] + knapsack_m(v, w, i-1, j-w[i-1], m))
        return m[i][j]
    cache = [[0 for j in range(len(w)+1)] for x in range(len(v)+1)]
    return knapsack_m(v, w, i, j, cache)

def knapsack_dp(v, w):
    table = [[0 for j in range(len(w)+1)] for x in range(len(v)+1)]
    for i in range(1, len(v)+1):
        for j in range(1, len(w)+1):
            if w[i-1] > j:
                table[i][j] = table[i-1][j]
            if w[i-1] <= j:
                table[i][j] = max(table[i-1][j], v[i-1] + table[i-1][j-w[i-1]])
    return table[len(v)][len(w)]

w = [3,6,2,7,8,4,2,3]
v = [1,4,7,4,7,7,4,6]
print(knapsack_r(w, v, len(v), len(w)))
print(knapsack_m(w, v, len(v), len(w)))
print(knapsack_dp(w, v))
