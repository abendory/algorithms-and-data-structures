# Enter your code here. Read input from STDIN. Print output to STDOUT

"""
            word     length
Geeks       0        5
for         1        3
Geeks       2        5
presents    3        8
word        4        4
wrap        5        4
problem     6        7

then we populate a 2d list with the cost of each word per line
m[0][0] = 1000
m[0][1] = 1
m[0][2] = INF

m[1][1] = 1728
m[1][2] = 216
m[1][3] = INF

m[2][2] = 1000
m[2][3] = 1
m[2][4] = INF

m[3][3] = 343
m[3][4] = 8
m[3][5] = INF

m[4][4] = 1331
m[4][5] = 216
m[4][6] = INF

m[5][5] = 1331
m[5][6] = 27

m[6][6] = 512

    0        1        2        3        4        5        6
0  1000      216     0        INF      INF      INF      INF
1            1782    216      INF      INF      INF      INF
2                    1000      1       INF      INF      INF
3                             343       8       INF      INF
4                                      1331     216      INF
5                                               1331      27
6                                                        512

    0        1          2             3            4                5        6
0  geeks geeks for geeks for geeks   INF           INF             INF      INF
1           for     for geeks        INF           INF             INF      INF
2                    geeks       geeks presents    INF             INF      INF
3                                   presents     presents word     INF      INF
4                                                  word         word wrap   INF
5                                                                wrap     wrap problem
6                                                                          problem

c[j] = min(c[i-1] + lc[i, j]) from 1<=i<=j
e.g.,
c[1] = min(c[0] + lc[1, 1])
c[2] = min(c[0] + lc[1, 2], c[1] + lc[2, 2])
c[3] = min(c[0] + lc[1, 3], c[1] + lc[2, 3], c[2] + lc[3, 3])
"""
def wordwrap(words, width):
    n = len(words)
    lc = [[-1 for j in range(n)] for i in range(n)]

    # two loops to create the 2d lc table
    # first loop stores the whitespace cost
    # second loop either cubes if > 0 or INFs if < 0
    for i in range(n):
        lc[i][i] = width - len(words[i])
        for j in range(i+1, n):
            print(i, j)
            lc[i][j] = lc[i][j-1] - len(words[j]) - 1

    for i in range(n):
        for j in range(i, n):
            if lc[i][j] < 0:
                lc[i][j] = float('inf')
            else:
                lc[i][j] = lc[i][j] ** 3

    # min_cost from i to n is found by trying
    # j between i to n and checking which
    # one has min value
    min_cost = [-1 for i in range(n)]
    result = [-1 for i in range(n)]
    # loop from last word to first word
    for i in range(n-1, -1, -1):
        min_cost[i] = lc[i][n-1]
        result[i] = n
        for j in range(n-1, i, -1):
            if lc[i][j-1] == float('inf'):
                continue
            if min_cost[i] > min_cost[j] + lc[i][j-1]:
                min_cost[i] = min_cost[j] + lc[i][j-1]
                result[i] = j

    print("Minimum cost is " + str(min_cost[0]))
    print()

    # gather the lines to print
    i = j = 0
    array = []
    while j < n:
        j = result[i]
        for k in range(i, j):
            array += (words[k] + ' ')
        array.append('\n')
        i = j
    string = ''.join(array)
    return string


string1 = ['Geeks', 'for', 'geeks', 'presents', 'word', 'wrap', 'problem.']
print(wordwrap(string1, 16))
# string2 = ['Lets', 'try', 'a', 'bunch', 'of', 'random', 'words', 'and', 'names', 'like', 'Addie', 'and', 'Gil.']
# print(wordwrap(string2, 16))
