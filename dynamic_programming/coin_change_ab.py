


def coin_rec(denominations, coins, change):
    if change <= denominations[-1] and coins[change]:
        return 1

    least = 1 + coin_rec(denominations, coins, change-1)
    for i in range(1, len(denominations)):
        if change - denominations[i] >= 1:
            least = min(least, 1 + coin_rec(denominations, coins, change-denominations[i]))
    return least

def coin_dp(denominations, coins, change):
    minCoins = [0] * (change + 1)
    for c in range(1, change+1):
        if c <= denominations[-1] and coins[c]:
            minCoins[c] = 1
            continue
        least = 1 + minCoins[c-1]
        for i in range(1, len(denominations)):
            if c - denominations[i] >= 1:
                least = min(least, 1 + minCoins[c - denominations[i]])
        minCoins[c] = least
    least = minCoins[change]
    return least


def main():
    denominations = [1, 3, 10, 25, 40]

    biggestCoin = denominations[-1]
    coins = [0] * (biggestCoin+1)

    for i in range(len(denominations)):
        coins[denominations[i]] = 1

    change = 41
    for i in range(1, change+1):
        print(i, coin_rec(denominations, coins, i), coin_dp(denominations, coins, i))

if __name__ == '__main__':
    main()
