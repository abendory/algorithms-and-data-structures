#!/usr/bin/env python

def wordbreak_rec(string, dictionary):
    if len(string) == 0: return ''

    for i in range(len(string)):
        prefix = string[:i+1]
        suffix = string[i+1:]
        if prefix in dictionary:
            wb = wordbreak_rec(suffix, dictionary)
            if wb is not None:
                return prefix + ' ' + wb

def wordbreak_rec_i(string, dictionary, s, e):
    if len(string) == 0: return ''

    for i in range(len(string)):
        s = i
        if string[i] in dictionary:
            wb = wordbreak_rec_i(string, dictionary, s+1, e)
            if wb is not None:
                return prefix + ' ' + wb

def wordbreak_dp(string, dictionary):
    print(string)
    if len(string) == 0: return ''
    if string in dictionary: return string

    n = len(string)

    table = [-1 for i in range(n)]

    #print('hi')
    for i in range(n):
        for j in range(i+1, n+1):
            if string[i:j] in dictionary:
                table[i] = len(string[i:j])
                break

    # create the final output string
    i, output = 0, []
    while i < len(table):
        if table[i] > -1:
            length = table[i]
            output.append(string[i:i+length])
            i += length

    return output

if __name__ == '__main__':
    dictionary = ["mobile", "samsung", "sam", "sung", "man", "mango", "icecream","and","go","i","like", "ice", "cream"]

    # print("Wordbreak Recursive")
    # print("-------------------")
    # print(wordbreak_rec("ilikesamsung", dictionary))
    # print(wordbreak_rec("iiiiiiii", dictionary))
    # print(wordbreak_rec("", dictionary))
    # print(wordbreak_rec("ilikelikeimangoiii", dictionary))
    # print(wordbreak_rec("samsungandmango", dictionary))
    # print(wordbreak_rec("samsungandmangok", dictionary))
    # print()
    print("Wordbreak Dynamic Programming")
    print("-----------------------------")
    print(wordbreak_dp("samsungandmangok", dictionary))
    print(wordbreak_dp("ilikesamsung", dictionary))
    print(wordbreak_dp("iiiiiiii", dictionary))
    print(wordbreak_dp("", dictionary))
    print(wordbreak_dp("ilikelikeimangoiii", dictionary))
    print(wordbreak_dp("samsungandmango", dictionary))

