def coinplay_r(array):
    def coinplay_r(array, i, j):
        if i == j: return array[i]
        if i == j-1: return max(array[i], array[j])
        return max(array[i] + min(coinplay_r(array, i+2, j), coinplay_r(array, i+1, j-1)),
                   array[j] + min(coinplay_r(array, i, j-2), coinplay_r(array, i+1, j-1)))
    return coinplay_r(array, 0, len(array)-1)

def coinplay_dp(array):
    m = len(array)
    cache = [[-1 for x in range(m)] for y in range(m)]

    for index in range(m):
        for i, j in enumerate(range(index, m)):
            x = cache[i+2][j] if i+2 <= j else 0
            y = cache[i+1][j-1] if i+1 <= j-1 else 0
            z = cache[i][j-2] if i <= j-2 else 0

            cache[i][j] = max(array[i] + min(x, y), array[j] + min(y, z));

    return cache[0][m-1]

print(coinplay_r([8,15,3,7,1]))
print(coinplay_dp([8,15,3,7,1]))



