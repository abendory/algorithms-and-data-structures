#!/usr/bin/env python3


# Naive
def fib(n):
    if n in (0, 1):
        return n
    return fib(n-2) + fib(n-1)


# Memoization
def fib_cache(n):
    def fib_cache(n, cache):
        if cache[n] is None:
            cache[n] = fib_cache(n-2, cache) + fib_cache(n-1, cache)
        return cache[n]

    cache = (n+1) * [None]
    cache[0], cache[1] = 0, 1
    return fib_cache(n, cache)

# DP with cache
def fib_dp(n):
    cache = (n+1) * [None]
    cache[0], cache[1] = 0, 1
    for i in range(2, n+1):
        cache[i] = cache[i-2] + cache[i-1]
    return cache[n]

# DP without cache
def fib_dp2(n):
    first, second = 0, 1
    for i in range(n-2):
        first, second = second, first+second
    return first + second

print(fib(10))        # T{O[n^2]}, S{O[n]}
print(fib_cache(10))  # T{O[n]}, S{O[n]}
print(fib_dp(10))    # T{O[n]}, S{O[n]}
print(fib_dp2(4))   # T{O[n]}, S{O[1]}
