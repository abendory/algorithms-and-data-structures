def len_long_incr_subseq(arr):
    def lis_(i, n, arr):
        if i == n-1:
            return 1
        top = 0
        for j in range(i+1, n):
            if(arr[i] < arr[j]):
                ans = lis_(j, n, arr)
                if ans > top:
                    top = ans
        return top + 1
    top = 0
    n = len(arr)
    for i in range(n):
        ans = lis_(i, n, arr)
        if ans > top:
            top = ans
    return top

def len_long_incr_subseq_memo(arr):
    def lis_(i, n, arr, cache):
        if cache[i] is None:
            if i == n-1:
                cache[i] = 1
            else:
                top = 0
                for j in range(i+1, n):
                    if arr[i] < arr[j]:
                        ans = lis_(j, n, arr, cache)
                        if ans > top:
                            top = ans
                cache[i] = top + 1
        return cache[i]
    top = 0
    n = len(arr)
    cache = [None] * n
    for i in range(n):
        ans = lis_(i, n, arr, cache)
        if ans > top:
            top = ans
    return top


def len_long_incr_subseq_reversed(arr):
    def lis_(i, arr):
        if i == 0:
            return 1
        top = 0
        for j in range(i):
            if arr[j] < arr[i]:
                ans = lis_(j, arr)
                if ans > top:
                    top = ans
        return top + 1
    top = 0
    n = len(arr)
    for i in range(n):
        ans = lis_(i, arr)
        if ans > top:
            top = ans
    return top


def len_long_incr_subseq_iterative(arr):
    top = 0
    n = len(arr)
    cache = [None] * n
    for i in range(n-1, -1, -1):
        top = 0
        for j in range(i+1, n):
            if arr[i] < arr[j] and cache[j] > top:
                top = cache[j]
        cache[i] = top + 1
    top = 0
    for i in range(n):
        if cache[i] > top:
            top = cache[i]
    return top


def len_long_incr_subseq_dp(arr):
    top = 0
    n = len(arr)
    cache = [None] * n
    for i in range(n):
        top = 0
        for j in range(i):
            if arr[j] < arr[i] and cache[j] > top:
                top = cache[j]
        cache[i] = top + 1
    top = 0
    for i in range(n):
        if cache[i] > top:
            top = cache[i]
    return top



arr = [0, 1, 2, 3, 4, 5]
print(len_long_incr_subseq_dp(arr))
arr = [-7, 10, 9, 2, 3, 8, 8, 1]
print(len_long_incr_subseq_dp(arr))
arr = [1, 5, 7, 8, 9]
print(len_long_incr_subseq_dp(arr))
arr = [20, 79, 51, 33, 108, 200]
print(len_long_incr_subseq_dp(arr))

