def mm_rec(p, i, j):
    if i == j: return 0
    min_count = float('inf')
    for index in range(i, j):
        count = (mm_rec(p, i, index) +
                 mm_rec(p, index+1, j) +
                 p[i-1]*p[j]*p[index])
        if count < min_count:
            min_count = count
    return min_count

def mm_memo(p, i, j):
    def mm_memo(p, i, j, m):
        if i == j: return 0
        if m[i][j]: return m[i][j]
        min_count = float('inf')
        for index in range(i, j):
            count = (mm_memo(p, i, index, m) +
                     mm_memo(p, index+1, j, m) +
                     p[i-1]*p[j]*p[index])
        if count < min_count:
            min_count = count
            m[i][j] = min_count
        return m[i][j]
    cache = [[0 for j in range(len(p)+1)] for i in range(len(p)+1)]
    return mm_memo(p, i, j, cache)


def mat_mult_dp(m):
    # 0th row and 0th column of m[][] are not used
    n = len(m)
    cache = [[0 for j in range(n)] for i in range(n)]

    # m[i,j] = Minimum number of scalar multiplications needed to compute
    # the matrix A[i]A[i+1]...A[j] = A[i..j] where dimension of A[i] is
    # p[i-1] x p[i]

    # L is chain length.
    for L in range(2, n):
        for i in range(1, n-L+1):
            j = i+L-1
            cache[i][j] = sys.maxsize
            for k in range(i, j):
                # q = cost/scalar multiplications
                q = cache[i][k] + cache[k+1][j] + m[i-1]*m[k]*m[j]
                if q < cache[i][j]:
                    cache[i][j] = q

    return cache[1][n-1]


arr = [2, 3, 5, 6]
print(mm_rec(arr, 1, len(arr)-1))
print(mm_memo(arr, 1, len(arr)-1))



