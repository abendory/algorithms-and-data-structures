#!/usr/bin/env python3

"""
 Model Classes that implement a basic deck of cards
 Author: Addie Bendory

"""


class PlayerHand:
    def __init__(self, cards):
        dealt_cards = cards

    def clear_hand(self):
        # called on new hands / reset of play

    def add_card(self, card):
        # adds a new card to the hand, called when deck calls deal_to_player

    def remove_card(self, card):
        # removes card from the hand, called when player burns or surrenders a card

    def sort(self, by_value=True, is_incrementing=True, is_ace_high=True):
        # sorts the deck by value
        # if by_value is false, will sort by suit
        # if is_incrementing is set, will sort in incrementing order, else decrementing


class Card:
    SPADE = 0
    HEART = 1
    CLUB = 2
    DIAMOND = 3
    JOKER = 4

    JACK = 11
    QUEEN = 12
    KING = 13
    ACE = 1

    def __init__(self, suit, value):
        self.suit  = suit       # club, diamond, heart, spade, joker
        self.value = value      # 2-10, 11-13, and 1 for J-A
        self.is_dealt = False   # bool to determine if card is still in the deck

class Deck:
    def __init__(self, cards):
        self.all_cards = cards  # list of all 52 std cards
        self.is_jokers = False  # are we going to use jokers

    def shuffle(self, num_hands, cards_per_hand):
        # shuffles the cards

    def deal_to_player(self, hand, is_dealer):
        # deals the cards, sets the is_dealt bool on dealt cards
        # if is_dealer, then treat differently for certain games, e.g. black jack

    def flip_card(self, number_of_cards):
        # deals the common cards, e.g., flop (poker),
        # may be used in other games to deal a common card(s)

    def remaining_cards(self):
        # checks to see num of cards that were not dealt

def main():
    standard_cards = []
    for value in range(1, 14):
        standard_cards.append(Card(CLUB, value))
    for value in range(1, 14):
        standard_cards.append(Card(DIAMOND, value))
    for value in range(1, 14):
        standard_cards.append(Card(SPADE, value))
    for value in range(1, 14):
        standard_cards.append(Card(HEART, value))

    deck = Deck(standard_cards)
