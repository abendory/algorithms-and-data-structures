#!/usr/bin/env python3

"""
 Model Classes that implement a basic file system
 Author: Addie Bendory

"""

from abc import ABCMeta


# MODEL: USER / BASEFILE / ACL

class User:

class Single(User):

class Group(User):

class ACL:

class BaseFile:
    # Abstract class
    __metaclass__ = ABCMeta

    def __init__(self, name, path, is_dir):
        self.name = name
        self.path = path
        self.parent_dir = parent_dir
        self.is_link = is_link
        self.link_path = link_path
        self.permissions =  # access control list [ACL]
        self.owner = owner
        self.group = group
        self.size = 0
        self.time_created = datetime.now()
        self.time_modified = datetime.now()
        self.is_visible = True

    def get_parent_dir(self):
        return this.parent_dir

    def chmod(self, mode):
        self.permissions = mode

    def chown(self, owner):
        self.owner = owner

    def copy(self, to):
        new_file = File(...)  # populate with same data
        new_file.path = to

    def move(self, to):
        self.path = to
        self.parent_dir = to.get_parent_dir()

    def rename(self, rename):
        self.name = rename


class File(BaseFile):
    def __init__(self, name, path):
        super.__init__(self, name, path)
        self.executable = False

class Folder(BaseFile):
    def __init__(self, name, path):
        super.__init__(self, name, path)
        self.children = []  # list of Files/Folders

# CONTROLLER

class FileSystem:

    def __init__(self):
        self.total = 1  # total files
        self.root = Folder('/', None, True)
        self.path = '/'

    def mount(self):

    def unmount(self):

    def open(self, file, permissions):
        # set r/w on open

    def close(self, file):

    def cd(self, new_dir):
        self.path = new_dir

    def execute(self, file):
        if file.executable == False:
            raise "Error: Not executable"
        else:
            execute_file(file)

    def pwd(self):
        print(self.path)

    def create_file(self, file, is_dir):
        self.total += 1
        if is_dir:
            f = Folder(name, get_current_path())
        else:
            f = File(name, get_current_path())
        return f

    def mkdir(self, name):
        d = self.create_file(name, True)
        return d

    def rmdir(self, name):


def main():
    filesystem = FileSystem():


