def expand_string(string, i, j):
    if i < 0 or j > len(string)-1 or string[i].lower() != string[j].lower():
        return i+1, j-1

    return expand_string(string, i-1, j+1)


def longest_palindrome(string):
    maxlen = maxleft = maxright = 0
    for i in range(len(string)):
        left, right = expand_string(string, i, i+1)
        if right-left > maxlen:
            maxlen, maxright, maxleft = right-left, right, left
        left, right = expand_string(string, i, i)
        if right-left > maxlen:
            maxlen, maxright, maxleft = right-left, right, left

    return string[maxleft:maxright+1]


print(longest_palindrome('asdfeadfehellothereerehtollehafefasfs'))
