"""
Minimum Window Substring
Given a string S and a string T, find the minimum window in S which will contain all the characters in T.
e.g.
S = "AYZABOBECODXBANC"
T = "ABC"
Minimum window is "BANC",which contains all letters - A B and C.
* If no such window exists, then return an empty string
* If there are multiple minimum windows of the same length, then return any one
* Characters may be repeated
"""



def min_win_substr(s, t):

    # initialize the count and the two dictionaries
    count = 0
    needtofind = {}
    for element in t:
        if element in needtofind:
            needtofind[element] += 1
        else:
            needtofind[element] = 1
    hasfound = {}
    for element in t:
        needtofind[element] = 0

    leftpos = rightpos = 0
    endpos = len(s)-1
    while rightpos <= endpos:
        while #dictionary is
            if t[rightpos] in needtofind:
                needtofind[item] -= 1
                if element in hasfound:
                    hasfound[element] += 1
                else:
                    hasfound[element] = 1
            else: continue


s = "AYZABOBECODXBANC"
t = ["A","B","C"]
