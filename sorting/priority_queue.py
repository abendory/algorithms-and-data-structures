""" Heap Implementation """

class PriorityQueue:

    def __init__(self, array=None):
        self.heap = array
        self.size = len(array) if array else 0
        self._heapify()

    def _sink(self, pos, endpos):
        childpos = 2*pos + 1
        while childpos <= endpos:
            if childpos+1 <= endpos and self.heap[childpos] < self.heap[childpos+1]:
                childpos += 1
            if self.heap[pos] < self.heap[childpos]:
                self.heap[pos], self.heap[childpos] = self.heap[childpos], self.heap[pos]
                pos = childpos
                childpos = 2*pos + 1
            else:
                return

    def _swim(self, pos):
        startpos = 0
        parentpos = (pos-1) >> 1
        while pos > startpos and self.heap[parentpos] < self.heap[pos]:
            self.heap[parentpos], self.heap[pos] = self.heap[pos], self.heap[parentpos]
            pos = parentpos
            parentpos = (pos-1) >> 1

    def _heapify(self):
        """create a priority queue"""
        for i in reversed(range(self.size//2)):
            self._sink(i, self.size-1)

    def insert(self, item):
        """insert a key into the priority queue"""
        self.heap.append(item)
        self.size += 1
        self._swim(self.size-1)

    def peek(self):
        """return the largest key"""
        if not self.is_empty():
            return self.heap[0]

    def pop(self):
        """return and remove the largest key"""
        if not self.is_empty():
            lastelt = self.heap.pop()
            if self.heap:
                returnitem = self.heap[0]
                self.heap[0] = lastelt
                self._sink(0, len(self.heap)-1)
            else:
                returnitem = lastelt
            self.size -= 1
            return returnitem

    def is_empty(self):
        """is the priority queue empty?"""
        return self.size == 0

    def get_size(self):
        """number of keys in the priority queue"""
        return self.size

    def sort(self):
        for end in range(len(self.heap)-1, 0, -1):
            if self.heap[0] > self.heap[end]:
                self.heap[end], self.heap[0] = self.heap[0], self.heap[end]
                self._sink(0, end-1)
        return self.heap


array = [3, 6, 9, 2, 4, 8, 1, 5, 7]
pq = PriorityQueue(array)
print('orig heap   ', pq.heap)
pq.insert(10)
print('insert big  ', pq.heap)
pq.insert(1)
print('insert small', pq.heap)
pq.insert(5)
print('insert med  ', pq.heap)
popped = pq.pop()
print('pop         ', popped, pq.heap)
peeked = pq.peek()
print('peek        ', peeked, pq.heap)
pq.sort()
print('sorted   ', pq.heap)
