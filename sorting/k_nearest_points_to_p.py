def eucl_dist(p, p1):
    return (p[0]-p1[0])**2 + (p[1]-p1[1])**2

def qsort(array):
    if len(array) <= 1: return array
    left, pivot, right = partition(array)
    return qsort(left) + pivot + qsort(right)

def partition(array):
    pivot, rest = array[0], array[1:]
    left = [x for x in rest if eucl_dist(x, point) < eucl_dist(pivot, point)]
    right = [x for x in rest if eucl_dist(x, point) > eucl_dist(pivot, point)]
    return left, [pivot], right


point = (4,3)
array = [(-2,5), (1,6), (2,3), (5,-3), (-4,2), (9,5), (2,1), (6,4), (2,2), (4,4)]
N = len(array)
K = 3
s_array = qsort(array)
for i in range(K):
    print(s_array[i])
