def quicksort(array):
    _quicksort(array, 0, len(array) - 1)

def _quicksort(array, start, end):
    if start < end:
        # partition the list
        pivot = partition(array, start, end)
        # sort both halves
        _quicksort(array, start, pivot-1)
        _quicksort(array, pivot+1, end)
    return array

def partition(array, start, end):
    pivot = array[start]
    left = start+1
    right = end
    done = False
    while not done:
        while left <= right and array[left] <= pivot:
            left += 1
        while array[right] >= pivot and right >=left:
            right -= 1
        if right < left:
            done = True
        else:
            array[left], array[right] = array[right], array[left]

    array[start], array[right] = array[right], array[start]

    return right


arr = [3,5,1,4,6,8,3,6,8,3,2]
quicksort(arr)
print(arr)
