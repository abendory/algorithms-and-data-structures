def distance(a1, a2):
    return abs(int(a2)-int(a1))

def qsort(array):
    if len(array) <= 1: return array
    left, pivot, right = partition(array)
    return qsort(left) + pivot + qsort(right)

def partition(array):
    pivot, rest = array[0], array[1:]
    left = [x for x in rest if x < pivot]
    right = [x for x in rest if x > pivot]
    return left, [pivot], right

def closestNumbers(n, s):
    ss = qsort(s)
    min_dist = float('inf')
    for i in range(n-1):
        dist = distance(ss[i], ss[i+1])
        if dist < min_dist:
            min_dist = dist
            nums = (ss[i], ss[i+1])
    return nums

s = '-20 -3916237 -357920 -3620601 7374819 -7330761 30 6246457 -6461594 266854'
s = list(map(int, s.split()))
print(closestNumbers(len(s), s))
