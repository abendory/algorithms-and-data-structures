def compare(x, y):
    """ Compares by MSB """
    a = int(str(x)+str(y))
    b = int(str(y)+str(x))

    if a < b: return -1
    if a == b: return 0
    if a > b: return 1

class Solution:
    # @param A : tuple of integers
    # @return a strings
    def largestNumber(self, A):
        n = len(A)
        A = list(A)
        A.sort(compare)
        return ''.join(map(str, A))

s = Solution()
print(s.largestNumber([8, 89]))
