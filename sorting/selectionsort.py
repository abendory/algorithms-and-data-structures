def selectionsort(a):
    n = len(a)
    for i in range(n):
        min = i
        for j in range(i+1, n):
            if a[j] < a[min]:
                min = j
        a[min], a[i] = a[i], a[min]


arr = [3,5,1,4,6,8,3,6,8,3,2]
selection_sort(arr)
print(arr)
