def quicksort(seq):
    if len(seq) <= 1:
        return seq
    lo, pi, hi = partition(seq)
    return quicksort(lo) + pi + quicksort(hi)

def partition(seq):
    pi, seq = seq[0], seq[1:]
    lo = [x for x in seq if x <= pi]
    hi = [x for x in seq if x > pi]
    return lo, [pi], hi

arr = [3,5,1,4,6,8,3,6,8,3,2]
arr2 = quicksort(arr)
print(arr2)
