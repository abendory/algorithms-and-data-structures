def heapify(a):
    """Transform list into a heap, in-place, in O(len(x)) time."""
    n = len(a)
    for i in reversed(range(n//2)):
        siftup(a, i)

def siftup(heap, pos):
    startpos = pos
    endpos = len(heap)
    newitem = heap[pos]
    # bubble up smaller child until you hit a leaf
    childpos = 2*pos + 1  # left child
    while childpos < endpos:
        # Set childpos to index of smaller child.
        rightpos = childpos + 1
        if rightpos < endpos and not heap[childpos] < heap[rightpos]:
            childpos = rightpos
        # Move the smaller child up.
        heap[pos] = heap[childpos]
        pos = childpos
        childpos = 2*pos + 1
    # The leaf at pos is empty now.  Put newitem there, and bubble it up
    # to its final resting place (by sifting its parents down).
    heap[pos] = newitem
    _siftdown(heap, startpos, pos)

def _siftdown(heap, startpos, pos):
    newitem = heap[pos]
    # Follow the path to the root, moving parents down until finding a place
    # newitem fits.
    while pos > startpos:
        parentpos = (pos - 1) >> 1
        parent = heap[parentpos]
        if newitem < parent:
            heap[pos] = parent
            pos = parentpos
            continue
        break
    heap[pos] = newitem

def heapsort(a):
    heapify(a)
    while()
        print

a = [33,64,87,34,27,42,96,12,61,17,31,76]
heapify(a)
print(a)
# [12, 17, 42, 33, 27, 76, 96, 34, 61, 64, 31, 87]
