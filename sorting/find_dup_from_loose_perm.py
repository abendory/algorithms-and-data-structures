"""
Duplicate in a loose permutation
Find a duplicated number in a loose permutation of numbers.  A permutation is an array that is size N, and also has positive numbers from 1 thru N.  A loose permutation is a permutation where some numbers are missing and some are duplicated, but the total number is still N.
 
* We want to find any one duplicated number; not necessarily the first or the least.
* It can occur anywhere in the input array, and we don't care how many times it's duplicated.
* Input array may nor may not be sorted.
* You can only use constant extra memory.
* There is no limit/constraint on N i.e it is a normal 4-byte integer.
 
e.g.
Input: 1,7,4,3,2,7,4: This array has 7 numbers from 1 thru 7, with some missing (5 and 6) and some duplicated (4 and 7). Albeit unsorted, but sorting is irrelevant to a permutation.
Output: 4 or 7
 
Input: 3,1,2:  This array has nothing missing.
Output: -1
 
Interview time: 30 minutes (This is a surprisingly aha! problem)
 
Solution: Aim for constant memory and linear run time. Think bucket sort.
 
About Test-cases: Test-cases only display one duplicated number. If that's not what your answer is, but your answer is still a duplicate number, then that's acceptable. 
 
Solution: https://soham.box.com/s/wte4tqbjbfocnhy76teobzki7sxi5lq8
"""

def findDuplicateFromPermutation(a):
    n = len(a)

    for i in range(n):
        if a[i] == i+1:
            i += 1
        else:
            while a[i] != i+1:
                pos = a[i]-1
                if a[i] == a[pos]:
                    return a[pos]
                a[i], a[pos] = a[pos], a[i]
    return -1


a = [1,7,4,3,2,7,4]
b = [1,2,3]
c = [7,7,4,8,1,1,5,6]
print(findDuplicateFromPermutation(a))
print(a)
print(findDuplicateFromPermutation(b))
print(findDuplicateFromPermutation(c))
