def mergesort(a):
    if len(a) <= 1:
        return a
    mid = len(a) // 2
    left = a[:mid]
    right = a[mid:]

    l = mergesort(left)
    r = mergesort(right)

    return merge(l, r)

def merge(left, right):
    res = []
    leftpos, rightpos = 0, 0
    while leftpos < len(left) and rightpos < len(right):
        if left[leftpos] < right[rightpos]:
            res.append(left[leftpos])
            leftpos += 1
        else:
            res.append(right[rightpos])
            rightpos += 1

    if left: res.extend(left[leftpos:])
    if right: res.extend(right[rightpos:])

    return res

array = [5, 9, 8, 3, 9, 8, 6, 8, 5, 4]

print("Unsorted array: ", array)
res = mergesort(array)
print("Sorted array:   ", res)
