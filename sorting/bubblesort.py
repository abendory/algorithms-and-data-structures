def bubblesort(a):
    n = len(a)
    for i in range(n):
        for j in range(n-1, i, -1):
            if (a[j] < a[j-1]):
                a[j], a[j-1] = a[j-1], a[j]

arr = [7,5,1,4,6,9,3,5,8,3,2]
bubblesort(arr)
print(arr)
