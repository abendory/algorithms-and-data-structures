def insertionsort(a):
    n = len(a)
    for i in range(n):
        j = i
        while a[j] < a[j-1] and j > 0:
            a[j], a[j-1] = a[j-1], a[j]
            j -= 1

arr = [3,5,1,4,6,8,3,6,8,3,2]
insertion_sort(arr)
print(arr)
