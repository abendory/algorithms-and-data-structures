"""
 Boolean Matrix Question
 Given an m x n matrix of 0s and 1s, if an element is 0, set its entire row and column to 0.
"""

class Solution:
    # @param A : list of list of integers
    # @return the same list modified
    def setZeroes(self, a):
        row_flag = False
        col_flag = False
        rows = len(a)      # num of rows
        cols = len(a[0])   # num of cols

        # check first row
        for j in range(cols):     # iterate through num of cols
            if a[0][j] == 0:
                row_flag = True

        # check first col
        for i in range(rows):     # iterate through num of rows
            if a[i][0] == 0:
                col_flag = True

        # check the remaining array
        for j in range(1, cols):
            for i in range(1, rows):
                if a[i][j] == 0:
                    a[0][j] = 0
                    a[i][0] = 0

        # set the array
        for j in range(1, cols):
            for i in range(1, rows):
                if a[0][j] == 0 or a[i][0] == 0:
                    a[i][j] = 0

        # update first row and first col
        if row_flag:
            for i in range(cols):
                a[0][i] = 0
        if col_flag:
            for i in range(rows):
                a[i][0] = 0

        return a

m = [[1,0,1],
     [1,1,1],
     [1,1,1]]
s = Solution()
a = s.setZeroes(m)
print(a)
