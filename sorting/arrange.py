def arrange(a):
    n = len(a)
    # First step: Increase all values by (a[a[i]]%n)*n
    for i in range(n):
        a[i] += (a[a[i]] % n) * n

    # Second Step: Divide all values by n
    for i in range(n):
        a[i] //= n


A = [1, 4, 0, 3, 2]
arrange(A)
print(A)
