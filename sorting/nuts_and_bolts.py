def match_nuts_and_bolts(nuts, bolts, low, high):
    if low < high:
        pivot = partition(nuts, bolts[low], low, high)
        partition(bolts, nuts[pivot], low, high)

        match_nuts_and_bolts(nuts, bolts, low, pivot-1)
        match_nuts_and_bolts(nuts, bolts, pivot+1, high)

def partition(arr, pivot, low, high):

    i = low
    for j in range(low, high):
        if arr[j] < pivot:
            arr[i], arr[j] = arr[j], arr[i]
            i += 1
        elif arr[j] == pivot:
            arr[j], arr[high] = arr[high], arr[j]
            j -= 1
    arr[i], arr[high] = arr[high], arr[i]

    #print(pivot, arr)
    return i


nuts = [2, 4, 6, 1, 5, 3]
bolts = [1, 5, 6, 3, 4, 2]
match_nuts_and_bolts(nuts, bolts, 0, len(nuts)-1)
print(nuts, bolts)
# def quicksort(array):
#     _quicksort(array, 0, len(array) - 1)

# def _quicksort(array, start, end):
#     if start < end:
#         # partition the list
#         pivot = partition(array, start, end)
#         # sort both halves
#         _quicksort(array, start, pivot-1)
#         _quicksort(array, pivot+1, end)
#     return array

# def partition(array, start, end):
#     pivot = array[start]
#     left = start+1
#     right = end
#     while right >= left:
#         while left <= right and array[left] <= pivot:
#             left += 1
#         while array[right] >= pivot and right >=left:
#             right -= 1
#         else:
#             array[left], array[right] = array[right], array[left]

#     array[start], array[right] = array[right], array[start]

#     return right

