class Node:
    def __init__(self, data=None, next=None):
        self.data = data
        self.next = next


def print_list(head):
    while head:
        print(head.data, end='->')
        head = head.next
    print()

def even_odd2(head):
    if not head:
        return

    odd = None
    even = None
    l1 = head
    while l1:
        if l1.data & 1:  # odd  #TODO split into a separate function
            if not odd:
                odd = l1
                l3 = odd
            else:
                l3.next = l1
                l3 = l3.next
        else:            # even
            if not even:
                even = l1
                l2 = even
            else:
                l2.next = l1
                l2 = l2.next
        l1 = l1.next

    if l2: l2.next = None
    if l3: l3.next = None
    return odd, even

def even_odd(head):
    end = head
    prev = None
    curr = head

    # get end node
    while end.next:
        end = end.next

    tail = end

    # Move all odd nodes
    #  before the first even node
    #  to after end
    while curr.data & 1 and curr is not end:
        tail.next = curr
        curr = curr.next
        tail.next.next = None
        tail = tail.next

    if curr.data & 1 == 0:
        head = curr
        while curr is not end:
            # skip even nodes
            if curr.data & 1 == 0:
                prev = curr
                curr = curr.next
            else:
                prev.next = curr.next  # skip odd node
                tail.next = curr
                tail.next.next = None
                tail = curr
                curr = prev.next
    else:
        prev = curr





head = Node(1, Node(2, Node(3, Node(4, Node(5, Node(6))))))
print_list(head)
even_odd(head)
