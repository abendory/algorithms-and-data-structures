class Node:
    def __init__(self, data):
        self.data = data
        self.next = None
        self.rand = None

def clone(head):
    temp = head
    while temp:
        next = temp.rand

        temp = temp.next

head = Node(1)
head.next = Node(2)
head.next.next = Node(3)
head.next.next.next = Node(4)
head.next.next.next.next = Node(5)
head.rand = head.next.next
head.next.rand = head
head.next.next.rand = head.next.next.next.next
head.next.next.next.rand = head.next.next
head.next.next.next.next.rand = head.next

clone(head)

# print the list
temp = head
while temp:
    print(temp.data)
    temp = temp.next
