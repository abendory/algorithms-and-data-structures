def hasMatchingParantheses(exp):
    valid = True
    n = len(exp)
    paren_stack = []
    brack_stack = []
    brace_stack = []
    for i in range(n):
        if exp[i] == '(':
            paren_stack.append(exp[i])
        elif exp[i] == '[':
            brack_stack.append(exp[i])
        elif exp[i] == '{':
            brace_stack.append(exp[i])
        elif exp[i] == ')':
            if paren_stack and paren_stack[-1] == '(':
                paren_stack.pop()
            else:
                return False
        elif exp[i] == ']':
            if brack_stack and brack_stack[-1] == '[':
                brack_stack.pop()
            else:
                return False
        elif exp[i] == '}':
            if brace_stack and brace_stack[-1] == '{':
                brace_stack.pop()
            else:
                return False

    if paren_stack or brack_stack or brace_stack:
        return False
    else:
        return True

e1 = '( ( 1 + 2 ) * 3 )'  # Output: True
e2 = '( { 1 + 2 ) * 3 )'  # Output: False
e3 = '( ( (1 + 2) * 3 ))' # Output: True
e4 = '{([])}'             # Output: True
e5 = '} ( 1 * 2) + 3 * ( 5 - 6)' # False

print(hasMatchingParantheses(e1))
print(hasMatchingParantheses(e2))
print(hasMatchingParantheses(e3))
print(hasMatchingParantheses(e4))
print(hasMatchingParantheses(e5))
