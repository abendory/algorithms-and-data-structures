# 1) Find if there's a loop in a linkedlist
# Solution use fast and slow pointer

# 2) Find the length of the loop
# Solution stop one pointer and continue moving second pointer until first=second while keeping a count

# 3) Find the beginning of the loop
# Solution once you have count, move first pointer to count and start second poitner at start, move both pointers until they meet


# 4) build a queue with fixed array

def is_full(self):
    return (self.tail + 1) % N == self.head

def enqueue(self, item):
    if not self.is_full():
        self.array[(++self.tail+1) % n] = item


# 5) minstack

# solution, use a auxilary stack to always push the min and pop whenever main stack is popped
