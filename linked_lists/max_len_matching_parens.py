def maxLenMatchingParen(exp):
    maxlen = 0
    countpos = 0
    s = []
    n = len(exp)
    
    for i in range(n):
        if exp[i] == '(':
            if not s:
                countpos = i
            s.append(i)
        else:
            if not s:
                countpos = i + 1
            else:
                s.pop()
                if not s:
                    start = countpos - 1
                else:
                    start = s[-1]
                if n > maxlen:
                    maxlen = n
    return maxlen
    
