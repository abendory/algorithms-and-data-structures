
# Naive array implementation for brevity
class Stack:
    def __init__(self):
        self.items = None
        self.minstack = None

    def pop(self):
        self.minstack.pop()
        return self.items.pop()

    def peek(self): return self.items[-1]

    def push(self, item):
        if self.items:
            self.items.append(item)
            current_min = self.minstack[-1]
            if item < current_min:
                current_min = item
            self.minstack.append(current_min)
        else:
            self.items = []
            self.minstack = []
            self.items.append(item)
            self.minstack.append(item)

    def get_minimum(self):
        return self.minstack[-1]



s = Stack()
a = [11, 4, 6, 7, 1, -99, 0, 2]
for i in range(len(a)):
    s.push(a[i])

print(s.peek())
print(s.get_minimum())
s.pop()
print(s.peek())
print(s.get_minimum())
s.pop()
print(s.peek())
print(s.get_minimum())
s.pop()
print(s.peek())
print(s.get_minimum())
s.pop()
print(s.peek())
print(s.get_minimum())
s.pop()
print(s.peek())
print(s.get_minimum())
s.pop()
print(s.peek())
print(s.get_minimum())
s.pop()


