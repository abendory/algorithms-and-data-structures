class Node:
    def __init__(self, data):
        self.data = data
        self.next = None

def print_list(head):
    if not head:
        return None
    while head:
        print(head.data, end='->')
        head = head.next

def reverse(head):
    prev = None
    current = head
    while current:
        next = current.next
        current.next = prev
        prev = current
        current = next
    return prev

def split_list(head):
    if not head:
        return head

    prev = None
    slowptr = fastptr = head
    while fastptr:
        prev = slowptr
        slowptr = slowptr.next
        if not fastptr.next: break
        fastptr = fastptr.next.next
    prev.next = None

    return slowptr

def zip(head):
    # split the list
    l1 = head
    l2 = split_list(head)

    # reverse second half
    l2 = reverse(l2)
    
    # link up elements
    while l2:
        next = l1.next
        l1.next = l2
        next2 = l2.next
        l2.next = next
        l1 = next
        l2 = next2

    return head
  
head = Node(1)
head.next = Node(2)
head.next.next = Node(3)
head.next.next.next = Node(4)
head.next.next.next.next = Node(5)
#head.next.next.next.next.next = Node(6)

print_list(zip(head))

