# Definition for singly-linked list.
class Node:
   def __init__(self, x, next=None):
       self.val = x
       self.next = next


def print_list(head):
    if not head:
        return None
    while head:
        print(head.val, end='->')
        head = head.next

def reverseBetween(A, m, n):
    if A is None or A.next is None:
        return A

    curr = A
    prev = None

    for _ in range(m-1):
        prev = curr
        curr = curr.next

    new_head = prev
    new_tail = curr

    for _ in range(n-m+1):
        next = curr.next
        curr.next = prev
        prev = curr
        curr = next

    if new_head: new_head.next = prev
    if new_tail: new_tail.next = curr

    if m == 1: return prev
    return A

l = Node(1, Node(2, Node(3, Node(4, Node(5)))))
h = reverseBetween(l, 1, 5)
print_list(h)
