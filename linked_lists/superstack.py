class Node:
    def __init__(self, item=None):
        self.item = item
        self.next = None

class SuperStack:
    def __init__(self, head=None):
        self.head = head
        self.size = 0

    def push(self, item):
        #print('push',item)
        if not self.head:
            self.head = Node(item)
        else:
            newhead = Node(item)
            newhead.next = self.head
            self.head = newhead
        self.size += 1

    def pop(self):
        #print('pop')
        if self.head:
            res = self.head
            if self.head.next == None:
                self.head = None
            else:
                self.head = self.head.next
            self.size -= 1
            return res
        else:
            return 'EMPTY'

    def inc(self, num, item):
        #print('inc',num,item)
        if num > self.size:
            return None
        start = self.head
        for i in range(self.size - num):
            start = start.next
        while start:
            start.item += item
            start = start.next

    def peek(self):
        if self.head:
            return self.head.item
        else:
            return 'EMPTY'

    def print_stack(self):
        if self.head:
            temp = self.head
            while temp:
                print(temp.item, end=' ')
                temp = temp.next
        print()

    def empty(self):
        return self.size == 0


inp = ['12', 'push 4', 'pop', 'push 3', 'push 5', 'push 2', 'inc 3 1', 'pop', 'push 1', 'inc 2 2', 'push 4', 'pop', 'pop']
s = SuperStack()
n = len(inp)
res = []
for i in range(1, n):
    a = inp[i].split()
    if a[0] == 'pop':
        s.pop()
    elif a[0] == 'push':
        s.push(int(a[1]))
    elif a[0] == 'inc':
        s.inc(int(a[1]), int(a[2]))
    res.append(s.peek())

print(res)

# OUTPUT
# push 4     [4]
# pop        EMPTY
# push 3     [3]
# push 5     [3, 5]
# push 2     [3, 5, 2]
# inc 3 1    [4, 6, 3]
# pop        [4, 6]
# push 1     [4, 6, 1]
# inc 2 2    [6, 8, 1]
# push 4     [6, 8, 1, 4]
# pop        [6, 8, 1]
# pop        [6, 8]

# HACKERRANK INPUT
# s = SuperStack()
# n = int(input())
# for i in range(n):
#     a = input().split()
#     if a[0] == 'pop':
#         s.pop()
#     elif a[0] == 'push':
#         s.push(int(a[1]))
#     elif a[0] == 'inc'
#         s.inc(int(a[1]), int(a[2]))
