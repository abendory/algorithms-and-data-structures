class Node:
    def __init__(self, data):
        self.data = data
        self.next = None
        self.prev = None

class DoublyLinkedList:
    def __init__(self):
        self.head = None
        self.tail = None
        self.size = 0

    def pop_head(self):
        if self.head:
            node = self.head
            if self.head == self.tail:
                self.head = self.tail = None
            else:
                self.head = self.head.next
            self.size -= 1
            return node

    def push_head(self, value):
        newnode = Node(value)
        if not self.head:
            self.head = self.tail = newnode
        else:
            self.head.prev = newnode
            newnode.next = self.head
            self.head = self.head.prev
        self.size += 1

    def pop_tail(self):
        if self.tail:
            node = self.tail
            if self.head == self.tail:
                self.head = self.tail = None
            else:
                self.tail = self.tail.prev
            self.size -= 1
            return node

    def push_tail(self, value):
        newnode = Node(value)
        if not self.head:
            self.head = self.tail = newnode
        else:
            self.tail.next = newnode
            newnode.prev = self.tail
            self.tail = self.tail.next
        self.size += 1
