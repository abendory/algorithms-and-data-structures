"""
Sliding Window Maximum
(Description courtesy of Leetcode, otherwise a popular problem)

A long array A[] is given to you. There is a sliding window of size w which is moving from the very left of the array to the very right. You can only see the w numbers in the window. Each time the sliding window moves rightwards by one position. Following is an example:
The array is [1 3 -1 -3 5 3 6 7], and w is 3.

Window position                Max
---------------               -----
[1  3  -1] -3  5  3  6  7       3
 1 [3  -1  -3] 5  3  6  7       3
 1  3 [-1  -3  5] 3  6  7       5
 1  3  -1 [-3  5  3] 6  7       5
 1  3  -1  -3 [5  3  6] 7       6
 1  3  -1  -3  5 [3  6  7]      7
Input: A long array A[], and a window width w
Output: An array B[], B[i] is the maximum value of from A[i] to A[i+w-1]
Requirement: Find a good optimal way to get B[i]
 """

from collections import deque
def sliding_max(A, w):
    n = len(A)
    B = []
    q = deque()

    # fills q with indices
    for i in range(w):
        while q and A[i] >= A[q[-1]]:
            q.pop()
        q.append(i)
    for i in range(w, n):
        B.append(A[q[0]])
        while q and A[i] >= A[q[-1]]:
            q.pop()
        while q and q[0] <= i-w:
            q.popleft()
        q.append(i)
    B.append(A[q[0]])
    return B

        
a = [2, 3, 4, 2, 6, 2, 5, 1]
w = 2
print(sliding_max(a, w))
