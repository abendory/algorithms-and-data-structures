class Node:
    def __init__(self, data):
        self.data = data
        self.next = None

def length(head):
    list_length = 0
    while head:
        head = head.next
        list_length += 1
    return list_length

def create_list_from_array(array):
    if not array:
        return None

    n = len(array)
    if n < 1:
        return None

    head = curr = Node(array[0])
    for i in range(1, n):
        curr.next = Node(array[i])
        curr = curr.next
    return head

def findmid(head):
    if not head:
        return head

    prev = None
    slowptr = fastptr = head
    while fastptr:
        prev = slowptr
        slowptr = slowptr.next
        if not fastptr.next: break
        fastptr = fastptr.next.next
    prev.next = None

    return slowptr

def mergesort(head):
    if not (head or head.next):
        return head

    return _mergesort(head, length(head))

def _mergesort(head, length):

    head2 = findmid(head)

    l1 = _mergesort(head, mid)
    l2 = _mergesort(head2, length-mid)
    return merge(l1, l2)

def merge(l1, l2):
    newhead, curr1, curr2 = None, l1, l2

    if l1 and l2 and curr1.data <= curr2.data or l1 and not l2:
        newhead = l1
        curr1 = curr1.next
    elif l1 and l2 and curr1.data > curr2.data or l2 and not l1:
        newhead = l2
        curr2 = curr2.next

    prev = newhead
    while curr1 and curr2:
        if curr1.data <= curr2.data:
            prev.next = curr1
            prev = curr1
            curr1 = curr1.next
        else:
            prev.next = curr2
            prev = curr2
            curr2 = curr2.next

    while curr1:
        prev.next = curr1
        prev = curr1
        curr1 = curr1.next

    while curr2:
        prev.next = curr2
        prev = curr2
        curr2 = curr2.next

    return newhead


array1 = [11, 4, 6, 7, 1, -99, 0, 2]
l1 = create_list_from_array(array1)

print(mergesort(l1))

array2 = [11, 8, -1]
l2 = create_list_from_array(array2)
print(mergesort(l2))

array3 = [1]
l3 = create_list_from_array(array3)
print(mergesort(l3))

array4 = [1, 2, 3, 4, 5]
l4 = create_list_from_array(array4)
print(mergesort(l4))

print(mergesort(None))

