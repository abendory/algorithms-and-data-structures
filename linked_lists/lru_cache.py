from structs import DoublyLinkedList, Node

class LRUCache:
    def __init__(self, capacity):
        self.capacity = capacity
        self.cache = {}
        self.cache_values = DoublyLinkedList()

    def set(self, key, value):
        if key in self.cache:
            print('exec')
            node = self.cache[key]
            node.data = value
            self.cache_values.pop_head(node)
            self.cache_values.push_tail(node)
        else:
            self.evict()
            node = Node(key, value)
            self.cache_values.push_tail(node)
            self.cache[key] = node

    def get(self, key):
        if key in self.cache:
            node = self.cache[key]
            self.cache_values.pop_head(node)
            self.cache_values.push_tail(node)
            return node.data
        else:
            return -1

    def evict(self):
        if self.cache_values.size >= self.capacity:
            node = self.cache_values.pop_head()
            del self.cache[node.key]

    def print_cache(self):
        node = self.cache_values.head
        while node:
            print(str(node.key) + ' ' + str(node.data))
            node = node.next

lru = LRUCache(4)
lru.set(key, value)
