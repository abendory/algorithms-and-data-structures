class Node:
    def __init__(self, data):
        self.data = data
        self.next = None

def print_list(head):
    if not head:
        return None
    while head:
        print(head.data, end='->')
        head = head.next

def reverse(head):
    prev = None
    current = head
    while current:
        next = current.next
        current.next = prev
        prev = current
        current = next
    head = prev
    return head

def reverse_rec(head):
    if not head or not head.next:
        return head

    newhead = reverse_rec(head.next)

    head.next.next = head
    head.next = None

    return newhead


head = Node(1)
head.next = Node(2)
head.next.next = Node(3)
head.next.next.next = Node(4)
head.next.next.next.next = Node(5)
head.next.next.next.next.next = Node(6)

head = reverse(head)
print_list(head)
print()

head = reverse_rec(head)
print_list(head)
