class Node:
    def __init__(self, data=None):
        self.data = data
        self.next = None

def alternativeSplit(l1):
    if not l1:
        return None

    newhead1 = l1
    n1 = newhead1
    newhead2 = n1.next
    n2 = newhead2

    while n1 and n1.next:
        n1.next = n1.next.next
        if n2.next:
            n2.next = n2.next.next
        n1 = n1.next
        n2 = n2.next

    return newhead1, newhead2

# Testcase 1

l1 = Node(1)
l1.next = Node(2)
l1.next.next = Node(3)
l1.next.next.next = Node(4)
l1.next.next.next.next = Node(5)

list1, list2 = alternativeSplit(l1)

print("testcase 1")
while list1:
    print(list1.data, end=' ')
    list1 = list1.next
print()

while list2:
    print(list2.data, end=' ')
    list2 = list2.next
print()

# Testcase 2

l1 = Node(47)
l1.next = Node(32)
l1.next.next = Node(12)
l1.next.next.next = Node(1)

list1, list2 = alternativeSplit(l1)

print("testcase 2")
while list1:
    print(list1.data, end=' ')
    list1 = list1.next
print()

while list2:
    print(list2.data, end=' ')
    list2 = list2.next
print()

# Testcase 3

l1 = Node(1)

list1, list2 = alternativeSplit(l1)

print("testcase 3")
while list1:
    print(list1.data, end=' ')
    list1 = list1.next
print()

while list2:
    print(list2.data, end=' ')
    list2 = list2.next
print()

# Testcase 4

l1 = Node()

list1, list2 = alternativeSplit(l1)

print("testcase 4")
while list1:
    print(list1.data, end=' ')
    list1 = list1.next
print()

while list2:
    print(list2.data, end=' ')
    list2 = list2.next
print()
