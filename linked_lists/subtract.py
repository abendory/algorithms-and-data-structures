"""
 Given a singly linked list, modify the value of first half nodes such that :
  1st node’s new value = the last node’s value - first node’s current value
  2nd node’s new value = the second last node’s value - 2nd node’s current value,
"""

# Definition for singly-linked list.
class ListNode:
    def __init__(self, x, next=None):
        self.val = x
        self.next = next

class Solution:
    # @param A : head node of linked list
    # @return the head node in the linked list
    def subtract(self, head):
        if not head: return
        if not head.next: return head

        curr = head
        half1, half2 = self.split(curr)
        half2 = self.reverse(half2)
        half1 = self.merge(half1, half2)
        half2 = self.reverse(half2)
        head = self.join(half1, half2)

        return head

    def split(self, head):
        curr = head
        slowptr = fastptr = head
        prev = None
        while fastptr and fastptr.next:
            prev = slowptr
            slowptr = slowptr.next
            fastptr = fastptr.next.next
        prev.next = None
        return head, slowptr

    def reverse(self, head):
        curr = head
        prev = None
        while curr:
            next = curr.next
            curr.next = prev
            prev = curr
            curr = next
        return prev

    def merge(self, head1, head2):
        curr1 = head1
        curr2 = head2
        while curr1 and curr2:
            curr1.val = curr2.val - curr1.val
            curr1 = curr1.next
            curr2 = curr2.next
        return head1

    def join(self, head1, head2):
        curr = head1
        while curr.next:
            curr = curr.next
        curr.next = head2
        return head1

def print_list(head):
    while head:
        print(head.val, end='->')
        head = head.next
    print()

head = ListNode(1, ListNode(2, ListNode(3, ListNode(4, ListNode(5)))))
s = Solution()
s.subtract(head)
#print('solution', end=' ')
#print_list(head)

