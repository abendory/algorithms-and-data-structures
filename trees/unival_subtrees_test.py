""" Print the number of unival subtrees in a given tree  """

from pretty import *

class Node:
    def __init__(self, v=None):
        self.val = v
        self.left = None
        self.right = None

def unival_subtrees(node):
    is_uv_subtree, count = _unival_subtrees(node)
    return count

def _unival_subtrees(node):
    if not node:
        return False, 0

    left, lcount = _unival_subtrees(node.left)
    right, rcount = _unival_subtrees(node.right)

    if left and right and \
              (node.left and node.left.val == node.val) and \
              (node.right and node.right.val == node.val):
        return True, lcount + rcount + 1

    if left and node.left and node.left.val == node.val:
        return True, lcount + 1

    if right and node.right and node.right.val == node.val:
        return True, rcount + 1

    return True, 1



root = Node(3)
root.left = Node(3)
root.left.left = Node(3)
root.left.right = Node(3)
root.right = Node(3)
print(unival_subtrees(root))
