# Definition for a  binary tree node
class TreeLinkNode:
    def __init__(self, x):
        self.val = x
        self.left = None
        self.right = None
        self.next = None

def inorder(root):
    if root:
        inorder(root.left)
        print(root.val, end=' ')
        inorder(root.right)

def inorder_next(root):
    if root:
        inorder(root.left)
        if root.next:
            print(root.next.val, end=' ')
        else:
            print('None', end=' ')
        inorder(root.right)

class Solution:
    # @param root, a tree node
    # @return nothing
    def connect(self, root):
        if not root:
            return

        n = root
        while n:
            p = n

            while p:
                if p.left:
                    if p.right:
                        p.left.next = p.right
                    else:
                        p.left.next = self.get_next(p)

                if p.right:
                    p.right.next = self.get_next(p)

                p = p.next

            if n.left: n = n.left
            elif n.right: n = n.right
            else: n = self.get_next(n)

        return root

    # get first node on next level
    def get_next(self, node):
        n = node.next
        while n:
            if n.left: return n.left
            if n.right: return n.right
            n = n.next

        return None


def form_complete_tree(depth, num):
    if depth == 0:
        return None
    root = TreeLinkNode(num)
    num += 1
    print('left',num)
    root.left = form_complete_tree(depth - 1, num);
    print('right',num)
    num += 1
    root.right = form_complete_tree(depth - 1, num);
    return root

s = Solution()
tree = form_complete_tree(3, 1)
#root = s.connect(tree)
inorder_next(tree)


