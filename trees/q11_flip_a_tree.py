#!/usr/local/bin/python3

from structs import Node, Stack, Queue
from structs import print_breadth_first


def flip(node):
    if not node:
        return
    if node.left:
        flip(node.left)
    if node.right:
        flip(node.right)

    if node.left and node.right:
        node.left, node.right = node.right, node.left
    elif node.left:
        node.left, node.right = None, node.left
    elif node.right:
        node.left, node.right = node.right, None

    return node


node = Node(35)
node.left  = Node(28)
node.left.left  = Node(1)
node.left.right = Node(29)
node.right = Node(45)
node.right.left  = Node(41)
node.right.left.right = Node(43)
node.right.right = Node(55)


print_breadth_first(node)
print()
print_breadth_first(flip(node))
