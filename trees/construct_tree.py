# Definition for a  binary tree node
class TreeNode:
    def __init__(self, x):
        self.val = x
        self.left = None
        self.right = None

class Solution:
    # @param A : integer
    # @return a list of TreeNode
    def construct_trees(self, n):
        start, end = 1, n
        return self._construct_trees(start, end)

    def _construct_trees(self, start, end):
        trees = []

        if start > end:
            return trees

        for i in range(start, end+1):
            lefttree = self._construct_trees(start, i-1)
            righttree = self._construct_trees(i+1, end)

            for j in range(len(lefttree)):
                left = lefttree[j]
                print(left.val)
                for k in range(len(righttree)):
                    right = righttree[k]
                    print(right.val)
                    node = TreeNode(i)
                    node.left = left
                    node.right = right
                    trees.append(node)

        return trees

s = Solution()
a = s.construct_trees(4)
print(a)
