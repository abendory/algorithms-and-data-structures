#!/usr/local/bin/python3

from structs import Node, Stack, Queue

"""
initialize: pathlen = 0, path[1000]
/*1000 is some max limit for paths, it can change*/

/*printPathsRecur traverses nodes of tree in preorder */
printPathsRecur(tree, path[], pathlen)
   1) If node is not NULL then
         a) push data to path array:
                path[pathlen] = node->data.
         b) increment pathlen
                pathlen++
   2) If node is a leaf node then print the path array.
   3) Else
        a) Call printPathsRecur for left subtree
                 printPathsRecur(node->left, path, pathLen)
        b) Call printPathsRecur for right subtree.
                printPathsRecur(node->right, path, pathLen)
"""

def paths(root, k):
    s = Stack()
    _print_all_paths(root, k, s, 0)

def _print_all_paths(root, k, s, total):
    if not root:
        return

    s.push(root.value)
    total += root.value

    if not root.left and not root.right:
        if total == k:
            print_path(s)
    else:
        print_all_paths(root.left, k, s)
        print_all_paths(root.right, k, s)

    s.pop()

def find_paths(root):
    def find_paths(root, paths=None):
        if not paths:
            paths = []

        # we're at a leaf, return the value
        if root.left is None and root.right is None:
            return [[root.value]]

        # if there's a left subtree, add the value plus recursive call to left subtree
        if root.left:
            paths.extend([[root.value] + subtree for subtree in find_paths(root.left)])

        # if there's a right subtree, add the value plus recursive call to right subtree
        if root.right:
            paths.extend([[root.value] + subtree for subtree in find_paths(root.right)])

        return paths
    return find_paths(root)


def print_all_paths(root, curpath, res):
    # append current value
    curpath.append(root.value)
    #print(curpath, res)

    # if we're at a leaf append the path
    if root.left is None and root.right is None:
        res.append(curpath)
        return

    print (curpath, res)
    # traverse left
    if root.left:
        print_all_paths(root.left, curpath, res)

    curpath.pop()

    # traverse right
    if root.right:
        print_all_paths(root.right, curpath, res)

    return res

if __name__ == '__main__':
    # Example binary search tree
    #    35
    #   /  \
    #  28   45
    #  /\   / \
    # 1 29 41 55
    #       \
    #        43

    tree = Node(35)
    tree.left  = Node(28)
    tree.left.left  = Node(1)
    tree.left.right = Node(29)
    tree.right = Node(45)
    tree.right.left  = Node(41)
    tree.right.left.right = Node(43)
    tree.right.right = Node(55)
    #print(find_paths(tree))

    result = []
    initpath = []
    print_all_paths(tree, initpath, result)
    # 35 28 1
    # 35 28 29
    # 35 45 41 43
    # 35 45 55
