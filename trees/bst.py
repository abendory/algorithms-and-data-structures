#!/usr/local/bin/python3

from structs import Node, Stack, Queue

# In order traversal O(n) time O(1) space
def morris_inorder(root):
    parent, tmp = root, None
    while None != parent:
        if None == parent.left:
            print(parent, end=' ')
            parent = parent.right
        else:
            tmp = parent.left
            while None != tmp.right and parent != tmp.right:
                tmp = tmp.right
            if None == tmp.right:
                tmp.right = parent
                parent = parent.left
            else:
                print(parent, end=' ')
                tmp.right = None
                parent = parent.right


def inorder_recursive(root):
    if root:
        inorder_recursive(root.left)
        print(root, end=' ')
        inorder_recursive(root.right)


def preorder_recursive(root):
    if root:
        print(root, end=' ')
        preorder_recursive(root.left)
        preorder_recursive(root.right)


def postorder_recursive(root):
    if root:
        postorder_recursive(root.left)
        postorder_recursive(root.right)
        print(root, end=' ')


def preorder_iterative(root):
    if root is None:
        return
    stack = Stack()
    stack.push(root)
    while not stack.is_empty():
        node = stack.pop()
        print(n)
        if node.right:
            stack.push(node.right)
        if node.left:
            stack.push(node.left)

def inorder_iterative(node):
    stack = []
    while stack or node:
        if node:
            stack.append(node)
            node = node.left
        else:
            node = stack.pop()
            print(node.value)
            node = node.right

# level order traversal
def bfs(node):
    if root is None:
        return
    queue = Queue()
    queue.enqueue(node)
    while(not queue.is_empty()):
        node = queue.dequeue()
        print(node, end=" ")
        if node.left:
            queue.enqueue(node.left)
        if node.right:
            queue.enqueue(node.right)

def print_by_level(node):
    if node is None:
        return
    queue = []
    queue.append(node)
    while queue:
        level = len(queue)
        while level:
            node = queue.pop(0)
            print(node, end=" ")
            if node.left: queue.append(node.left)
            if node.right: queue.append(node.right)
            level -= 1
        print()

def sum(root):
    if root is None:
        return None
    left = right = 0
    if root.left is not None:
        left = sum(root.left)
    if root.right is not None:
        right = sum(root.right)
    return root.value + left + right


def mirror(root):
    if root is None:
        return None

    mirror(root.left)
    mirror(root.right)

    if root.left and root.right:
        root.left, root.right = root.right, root.left
    elif root.left:
        root.right = root.left
        root.left = None
    elif root.right:
        root.left = root.right
        root.right = None

def add_left(root):
    """duplicates the left child of every node"""
    if root is None:
        return

    add_left(root.left)
    add_left(root.right)

    temp = Node(root.value)
    temp.left = root.left
    root.left = temp


def insert(root, val):
    """inserts val into tree maintaining BST property"""
    if root is None:
        root = Node(val)
    else:
        if root.value > val:
            root.left = insert(root.left, val)
        else:
            root.right = insert(root.right, val)

    return root


def minimum(root):
    if root.left:
        return minimum(root.left)
    else:
        return root


def successor(root, node):
    """TODO: return the next largest value"""
    # min of right subtree OR go up until parent.val > value
    if node.right:
        return minimum(node.right)

    # from root go down until root == node while tracking parent (successor)
    succ = None
    while root:
        if node.value < root.value:
            succ = root
            root = root.left
        elif node.value > root.value:
            root = root.right
        else: break
    return succ


def delete(root, node):
    """TODO: delete a value"""
    # find successor and copy to deleted node
    # then call delete on original successor
    while root:
        if node.value < root.value
            root = root.left
        elif node.value > root.value
            root = root.right
        else:
            if root.left and root.right: # if both children are present
                succ = minimum(root.right)
                root.value = succ.value
                successor.binary_tree_delete(successor.key)
            elif self.left_child:   # if the node has only a *left* child
                self.replace_node_in_parent(self.left_child)
            elif self.right_child:  # if the node has only a *right* child
                self.replace_node_in_parent(self.right_child)
            else: # this node has no children
                self.replace_node_in_parent(None)






def find_kth_value(root, k):
    # if root is the answer then there are k-1 nodes in the left subtree
    # if answer is in left k <= size(left)
    # if answer is in right k > size(left)+1
    pass


if __name__ == '__main__':
    # Example binary search tree
    #    35
    #   /  \
    #  28   45
    #  /\   / \
    # 1 29 41 55
    #       \
    #        43

    # VALID BST
    root = Node(35)
    root.left  = Node(28)
    root.left.left  = Node(1)
    root.left.right = Node(29)
    root.right = Node(45)
    root.right.left  = Node(41)
    root.right.left.right = Node(43)
    root.right.right = Node(55)
    delete(root, root.left.right)



    # NOT A BST
    #    35
    #   /  \
    #  22   45
    #  /\   / \
    # 1 20 46 55
    #       \
    #        43

    root2 = Node(35)
    root2.left  = Node(22)
    root2.left.left  = Node(1)
    root2.left.right = Node(20)
    root2.right = Node(45)
    root2.right.left  = Node(46)
    root2.right.left.right = Node(43)
    root2.right.right = Node(55)

    # Test Functions
    # dfs(n)
    # bfs(n)
    # sum(n)
    # add_left(n)
    # minimum(n)
    # insert(n, 42)
    # print(successor(n, n.left.left))
    # post_order_iterative(n)
    # postorder_recursive(n)
    # inorder_recursive(n)
    # morris_inorder(n)
    # print(is_bst(n))  # --> True
    # print(is_bst(n2))  # --> False
    # print(is_bst_inorder(n))  # --> True
    # print(is_bst_inorder(n2))  # --> False

