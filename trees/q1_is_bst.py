#!/usr/local/bin/python3

from structs import Node, Stack, Queue

INF = float('inf')

# METHOD ONE
# Recursively check subtrees
def is_bst(root):
    return is_bst_helper(root, -INF, INF)  # min int and max int

def is_bst_helper(root, minValue, maxValue):
    if root is None:
        return True
    if (root.value > minValue and root.value < maxValue and
        is_bst_helper(root.left, minValue, root.value) and
        is_bst_helper(root.right, root.value, maxValue)):
        return True
    return False

# METHOD TWO
# Implement an in-order traversal
def static_vars(**kwargs):
    """decorator to make static variable 'prev' """
    def decorate(func):
        for k in kwargs:
            setattr(func, k, kwargs[k])
        return func
    return decorate

@static_vars(prev=None)
def is_bst_inorder(root):
    if root:
        if not is_bst_inorder(root.left):
            return False
        if is_bst_inorder.prev is not None and root.value <= is_bst_inorder.prev.value:
            return False

        is_bst_inorder.prev = root

        return is_bst_inorder(root.right);

    return True


if __name__ == '__main__':
    # Example binary search tree
    #    35
    #   /  \
    #  28   45
    #  /\   / \
    # 1 29 41 55
    #       \
    #        43

    tree1 = Node(35)
    tree1.left  = Node(28)
    tree1.left.left  = Node(1)
    tree1.left.right = Node(29)
    tree1.right = Node(45)
    tree1.right.left  = Node(41)
    tree1.right.left.right = Node(43)
    tree1.right.right = Node(55)


    # Example non-binary search tree
    #    35
    #   /  \
    #  22   45
    #  /\   / \
    # 1 20 23 55
    #       \
    #        22

    tree2 = Node(35)
    tree2.left  = Node(22)
    tree2.left.left  = Node(1)
    tree2.left.right = Node(20)
    tree2.right = Node(45)
    tree2.right.left  = Node(23)
    tree2.right.left.right = Node(22)
    tree2.right.right = Node(55)

    print(is_bst(tree1))  # => True
    print(is_bst(tree2))  # => False
    print(is_bst_inorder(tree1))  # => True
    print(is_bst_inorder(tree2))  # => False
