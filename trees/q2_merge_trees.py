#!/usr/local/bin/python3

from structs import Node, Stack, Queue

# Algorithm
# 1. Inorder traversal of both trees to get ordered arrays
# 2. Merge arrays into final array
# 3. Reconstruct tree from final array
# 4. Print tree by level

def inorder_traverse(node):
    if node is None:
        return

    stack = []
    res = []
    while stack or node:
        if node:
            stack.append(node)
            node = node.left
        else:
            node = stack.pop()
            res.append(node.value)
            node = node.right
    return res

def merge_arrays(first_array, second_array):
    """merges two inorder arrays keeping them in order"""
    i = 0
    j = 0
    merged_array = []

    while i < len(first_array) and j < len(second_array):
        if first_array[i] <= second_array[j]:
            merged_array.append(first_array[i])
            i += 1
        else:
            merged_array.append(second_array[j])
            j += 1

    while i < len(first_array):
        merged_array.append(first_array[i])
        i += 1

    while j < len(second_array):
        merged_array.append(second_array[j])
        j += 1

    return merged_array

def sorted_array_to_bst(array):
    """binary search to convert sorted
       array into a binary search tree"""
    if len(array) <= 1:
        return array

    return sorted_array_to_bst_helper(array, 0, len(array)-1)

def sorted_array_to_bst_helper(array, start, end):
    if start > end:
        return
    mid = (start + end) // 2
    root = Node(array[mid])
    root.left = sorted_array_to_bst_helper(array, start, mid-1)
    root.right = sorted_array_to_bst_helper(array, mid+1, end)

    return root

def print_breadth_first(root):
    if not root:
        return
    queue = []
    current_nodes = 1
    next_nodes = 0
    queue.append(root)
    while queue:
        node = queue.pop(0)
        print(node, end=' ')
        current_nodes -= 1
        if node.left:
            queue.append(node.left)
        if node.right:
            queue.append(node.right)
        next_nodes += 2
        if current_nodes == 0:
            print()
            current_nodes = next_nodes
            next_nodes = 0

if __name__ == '__main__':
    tree1 = Node(35)
    tree1.left  = Node(28)
    tree1.left.left  = Node(1)
    tree1.left.right = Node(29)
    tree1.right = Node(45)
    tree1.right.left  = Node(41)
    tree1.right.left.right = Node(43)
    tree1.right.right = Node(55)

    tree2 = Node(12)
    tree2.left  = Node(10)
    tree2.left.left  = Node(2)
    tree2.right = Node(44)
    tree2.right.left = Node(37)
    tree2.right.right = Node(56)

    print_breadth_first(sorted_array_to_bst(merge_arrays(inorder_traverse(tree1), inorder_traverse(tree2))))

