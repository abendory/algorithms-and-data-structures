class Node:
    def __init__(self, value=None, left=None, right=None):
        self.value = value
        self.left = left
        self.right = right

    def __repr__(self):
        return str(self.value)

class Queue:
    def __init__(self): self.items = []
    def __str__(self): return str(self.items)
    def __len__(self): return len(self.items)
    def is_empty(self): return self.items == []
    def enqueue(self, item): self.items.insert(0, item)
    def dequeue(self): return None if self.is_empty() else self.items.pop()

class Stack:
    def __init__(self): self.items = []
    def __str__(self): return str(self.items)
    def __len__(self): return len(self.items)
    def is_empty(self): return (self.items == [])
    def push(self, item): self.items.append(item)
    def pop(self): return None if self.is_empty() else self.items.pop()
    def peek(self): return None if self.is_empty() else self.items[len(self.items)-1]

def print_breadth_first(root):
    if not root:
        return
    queue = []
    current_nodes = 1
    next_nodes = 0
    queue.insert(0, root)
    while queue:
        node = queue.pop()
        print(node, end=' ')
        current_nodes -= 1
        if node.left:
            queue.insert(0, node.left)
        if node.right:
            queue.insert(0, node.right)
        next_nodes += 2
        if current_nodes == 0:
            print()
            current_nodes = next_nodes
            next_nodes = 0
    print()
