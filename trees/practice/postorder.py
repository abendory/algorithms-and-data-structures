#!/usr/bin/env python3
from structs import Node

def postorder(node):
    if not node:
        return

    s = []
    s.append(node)  # push
    visited = None
    while s:
        n = s[-1]  # peek
        visited_subtree =  n.left == visited or n.right == visited
        is_leaf = not n.left and not n.right
        if visited_subtree or is_leaf:
            n =  s.pop()
            print(n.value)
            visited = n
        else:
            if n.right: s.append(n.right)
            if n.left: s.append(n.left)

tree = Node(35)
tree.left  = Node(28)
tree.left.left  = Node(1)
tree.left.right = Node(29)
tree.right = Node(45)
tree.right.left  = Node(41)
tree.right.left.right = Node(43)
tree.right.right = Node(55)

postorder(tree)
