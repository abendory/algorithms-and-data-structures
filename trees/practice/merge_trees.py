from structs import Node, Stack, Queue

def inorder(n):
    if not n:
        return None

    res = []
    s = Stack()
    while s or n:
        if n:
            s.push(n)
            n = n.left
        else:
            n = s.pop()
            res.append(n.value)
            n = n.right
    return res


def inorder_iterative(node):
    stack = []
    while stack or node:
        if node:
            stack.append(node)
            node = node.left
        else:
            node = stack.pop()
            print(node.value)
            node = node.right

def merge(a, b):
    if not a and not b: return None
    if not a: return b
    if not b: return a

    res = []
    i = j = 0
    while i < len(a) and j < len(b):
        if a[i] <= b[j]:
            res.append(a[i])
            i += 1
        else:
            res.append(b[j])
            j += 1

    if i < len(a): res.extend(a[i:])
    if j < len(b): res.extend(b[j:])

    return res


def to_bst(array):
    if len(array) < 1:
        return
    if len(array) == 1:
        return Node(array[0])

    start, end = 0, len(array) - 1
    return _to_bst(array, start, end)

def _to_bst(array, start, end):
    if start > end:
        return

    mid = (start + end) // 2
    root = Node(array[mid])

    root.left = _to_bst(array, start, mid-1)
    root.right = _to_bst(array, mid+1, end)

    return root


def print_bf(node):
    if node is None:
        return
    q = Queue()
    q.enqueue(node)
    while q:
        node = q.dequeue()
        print(node, end=" ")
        if node.left:
            q.enqueue(node.left)
        if node.right:
            q.enqueue(node.right)


tree1 = Node(35)
tree1.left  = Node(28)
tree1.left.left  = Node(2)
tree1.left.left.left  = Node(1)
tree1.left.right = Node(29)
tree1.right = Node(45)
tree1.right.left  = Node(41)
tree1.right.left.right = Node(43)
tree1.right.right = Node(55)

tree2 = Node(12)
tree2.left  = Node(10)
tree2.left.left  = Node(3)
tree2.right = Node(44)
tree2.right.left = Node(37)
tree2.right.right = Node(56)

print_bf(to_bst(merge(inorder(tree1), inorder(tree2))))
