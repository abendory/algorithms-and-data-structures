#!/usr/local/bin/python3

from structs import Node, Stack, Queue

def pprint(array, length):
    for i in range(length):
        print(array[i], end=' ')
    print()

def print_path(root):
    path = [None] * 100
    _print_path(root, path, 0)

def _print_path(root, path, length):
    if not root:
        return

    print('Debug:', root.value, length)

    if not path: path = []
    path[length] = root.value
    length += 1

    if not root.left and not root.right:
        pprint(path, length)
    else:
        _print_path(root.left, path, length)
        _print_path(root.right, path, length)


if __name__ == '__main__':

    root = Node(35)
    root.left  = Node(28)
    root.left.left  = Node(1)
    root.left.right = Node(29)
    root.right = Node(45)
    root.right.left  = Node(41)
    root.right.left.right = Node(43)
    root.right.right = Node(55)

    print_path(root)

