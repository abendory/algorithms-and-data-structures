from structs import Node

INF = float('inf')

def is_bst(root):
    return _is_bst(root, -INF, INF)

def _is_bst(root, low, high):
    if not root:
        return True

    left = _is_bst(root.left, low, root.value)
    right = _is_bst(root.right, root.value, high)

    if root.value >= low and root.value <= high and left and right:
        return True

    return False



n = Node(35,
        Node(28,
            Node(1, None, None),
            Node(29, None, None)),
        Node(45,
            Node(41,
                None,
                Node(43, None, None)),
            Node(55, None, None)))


print(is_bst(n))
