#!/usr/local/bin/python3

from structs import Node, Stack, Queue

def postorder_iterative(root):
    if root is None:
        return
    s = Stack()
    s.push(root)
    visited = None
    while not s.is_empty():
        n = s.peek()
        visited_both_subtrees = (n.right == visited or n.left == visited)
        is_leaf = n.left is None and n.right is None
        if (visited_both_subtrees or is_leaf):
            s.pop()
            print(n.value, end=' ')
            visited = n
        else:
            if n.right: s.push(n.right)
            if n.left: s.push(n.left)

if __name__ == '__main__':
    # Example binary search tree
    #    35
    #   /  \
    #  28   45
    #  /\   / \
    # 1 29 41 55
    #       \
    #        43

    tree = Node(35)
    tree.left  = Node(28)
    tree.left.left  = Node(1)
    tree.left.right = Node(29)
    tree.right = Node(45)
    tree.right.left  = Node(41)
    tree.right.left.right = Node(43)
    tree.right.right = Node(55)

    postorder_iterative(tree)
