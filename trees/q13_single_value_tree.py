#!/usr/local/bin/python3

from structs import Node, Stack, Queue
from structs import print_breadth_first

"""
go to top
call same func on left and right
we want to avoid calling all the way to leaf
so we can try bottom up
base case, if you're a leaf node then you're an SVT
go to parent
ex
root 2
left 2
right 2
traverse to left, check value, val = 2 count = 1 because it's a leaf
traverse to right, check value, val = 2 count = 1 because it's a leaf
leftcount = is_single_value_tree(root.left)
rightcount = is_single_value_tree(root.right)
then check if parent val = right and left then count += 1
"""

# Method 1
def is_single_value_tree(node):
    if not node:
        return True

    key = node.value
    return is_single_value_tree_helper(node, key)

def is_single_value_tree_helper(node, key):
    if not node:
        return True

    return (node.value == key and
            is_single_value_tree_helper(node.left, key) and
            is_single_value_tree_helper(node.right, key))


def count_single_value_subtrees(node):
    return count_single_value_subtrees_helper(node, 0)

def count_single_value_subtrees_helper(node, counter):
    if not node:
        return

    if(is_single_value_tree(node)):
        counter += 1
    count_single_value_subtrees_helper(node.left, counter)
    count_single_value_subtrees_helper(node.right, counter)
    return counter

# Method 2

# Function takes a root node an integer pointer
# which stores the total count of unival subtrees
def count_optimized(node, counter):

    if not root:
        return True

    l = count_optimized(root.left, counter)
    r = count_optimized(root.right, counter)

    # both left and right subtrees are unival
    if l and r:
        rl = root.left
        rr = root.right

        # if leaf node OR left and right child exists and their data is also same as root's data OR only left child exists and its data is same as root's data OR only right child exists and its data is same as root's data
        if not rl and not rr or \
           rl and rr and rl.data == root.data and rr.data == root.data or \
           rl and rl.data == root.data or \
           rr and rr.data == root.data:

            counter += 1
            return True

    return False



root = Node(1)
root.left = Node(2)
root.left.left = Node(2)
root.left.right = Node(2)
root.left.left.left = Node(5)
root.left.left.right = Node(5)
root.right = Node(3)
root.right.left = Node(3)
root.right.right = Node(3)
root.right.left.left = Node(4)
root.right.left.right = Node(4)
root.right.right.left = Node(3)
root.right.right.right = Node(3)

print(count_single_value_subtrees(root))
