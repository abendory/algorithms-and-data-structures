#!/usr/local/bin/python3

from structs import Node, Stack, Queue

"""
Least Common Ancestor (LCA)

Let T be a rooted tree. The lowest common ancestor between two nodes n1 and n2 is defined as the lowest node in T that has both n1 and n2 as descendants.

The LCA of n1 and n2 in T is the shared ancestor of n1 and n2 that is located farthest from the root. Computation of lowest common ancestors may be useful, for instance, as part of a procedure for determining the distance between pairs of nodes in a tree: the distance from n1 to n2 can be computed as the distance from the root to n1, plus the distance from the root to n2, minus twice the distance from the root to their lowest common ancestor. (Source: Wikipedia)

Design an write an algorithm to find the LCA node, given two nodes in a Binary Tree.
* The tree may or may not be a BST
* Assume a Node structure that has NO parent pointer
* Assume that the two nodes are distinct and exist in the tree
* Find a solution that has runtime complexity of O(N). N is # nodes in the tree.

Desired solution: O(N) time.
"""

# think of this as a postorder traversal
# return the root when it is equal to n1 or n2
# return the root when it is equal to n1 and n2 is null and vice versa

# Algorithm
# ---------
# Store path to each node n1, n2 in an array
# LCA is last node before the paths differ

def least_common_ancestor(root, key1, key2):
    if not root:
        return None

    key = root.value

    if key == key1 or key == key2:
        return root

    # make sure key1 < key2
    key1, key2 = (key2, key1) if key2 < key1 else (key1, key2)

    left = least_common_ancestor(root.left, key1, key2)
    right = least_common_ancestor(root.right, key1, key2)
    if left and right:
        return root

    return left or right

def find_path(root, stack, value):
    if not root:
        return
    stack = Stack()
    stack.push(root)
    while(not stack.is_empty()):
        node = stack.pop()
        if node.left:
            stack.push(node.left)
        if node.right:
            stack.push(node.right)

def find_lca(root, node1, node2):
    path1 = find_path(root, node1)
    path2 = find_path(root, node2)

    i = 0
    length = min(len(path1[0]), len(path2[0]))
    while i < length-1 and path1[0][i] == path2[0][i]:
        lca = path1[0][i]
        i += 1

    return lca


if __name__ == '__main__':

    node = Node(45)
    node.left  = Node(25)
    node.left.left  = Node(15)
    node.left.left.left   = Node(10)
    node.left.left.right  = Node(20)
    node.left.right = Node(30)
    node.right = Node(65)
    node.right.left  = Node(55)
    node.right.left.left  = Node(50)
    node.right.left.right = Node(60)
    node.right.right = Node(75)
    node.right.right.right = Node(80)

    #print(find_path(node, 15))
    #print(find_lca(node, 10, 20)) # = 15
    #print(find_lca(node, 50, 80)) # = 65
    #print(find_lca(node, 20, 60)) # = 45
    print(least_common_ancestor(node, 10, 80))
