#!/usr/local/bin/python3

from structs import Node, Stack, Queue

# Example
# -------
# inorder_array = [10, 30, 40, 50, 60, 70, 90]
# preorder_array = [50, 30, 10, 40, 70, 60, 90]

# root = [50]
# root_left_subtree = [10, 30, 40]
# root_right_subtree = [60, 70, 90]

# root_left = [30]
# root_left_left_subtree = [10]
# root_left_right_subtree = [40]

# root_right = [70]
# root_right_left_subtree = [60]
# root_right_right_subtree = [90]

def binary_search():


def rebuild_tree(inord, preord):
    return rebuild_tree_helper(inord, 0, len(inord)-1,
                               preord, 0, len(preord)-1)

def rebuild_tree_helper(inord, inord_start, inord_end,
                        preord, preord_start, preord_end):
    if inord_start > inord_end:
        return

    # set the root value to the first element in the preorder array
    root_value = preord[preord_start]
    root_index = 0

    # get the index of the root in the inorder array
    binary_search()

    for index, value in enumerate(inord):
        if value == root_value:
            root_index = index
            break

    root = Node(root_value)
    length = root_index - inord_start

    root.left = rebuild_tree_helper(inord, inord_start, root_index-1,
                              preord, preord_start+1, preord_start+length)
    root.right = rebuild_tree_helper(inord, root_index+1, inord_end,
                              preord, preord_start+length+1, preord_end)
    return root

def breadth_first_traverse(root):
    if not root:
        return
    queue = []
    current_nodes = 1
    next_nodes = 0
    queue.insert(0, root)
    while queue:
        node = queue.pop()
        print(node, end=' ')
        current_nodes -= 1
        if node.left:
            queue.insert(0, node.left)
        if node.right:
            queue.insert(0, node.right)
        next_nodes += 2
        if current_nodes == 0:
            print()
            current_nodes = next_nodes
            next_nodes = 0


if __name__ == '__main__':
    tree1 = Node(35)
    tree1.left  = Node(28)
    tree1.left.left  = Node(1)
    tree1.left.right = Node(29)
    tree1.right = Node(45)
    tree1.right.left  = Node(41)
    tree1.right.left.right = Node(43)
    tree1.right.right = Node(55)


    inorder_array = [10, 30, 40, 50, 60, 70, 90]
    preorder_array = [50, 30, 10, 40, 70, 60, 90]
    root = rebuild_tree(inorder_array, preorder_array)
    breadth_first_traverse(root)
    # print(root, root.left, root.right, root.left.left, root.left.right, root.right.left, root.right.right)

    # print(tree1, tree1.left, tree1.right, tree1.left.left, tree1.left.right, tree1.right.left, tree1.right.right)

    # Solution
    # 50
    # 30 70
    # 10 40 60 90
    #
    # Binary search tree
    #     50
    #    /   \
    #  30    70
    #  /\    / \
    # 10 40 60 90
