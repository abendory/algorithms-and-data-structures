""" Write a function that takes an unsigned integer
    and returns the number of 1 bits it has. """

class Solution:
    # @param A : integer
    # @return an integer
    def numSetBits(self, A):
        bits = 0
        while A != 0:
            A &= A-1
            bits += 1

        return bits


# x & (x-1) will clear the LSB set bit in x
# e.g., x   1000100 &
#       x-1 1000011
#           -------
#           1000000
