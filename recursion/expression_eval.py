"""
EXPRESSION EVALUATOR

Given a string of integers as input, put between each pair of digits, one of {“”, “*”, “+”} such that the expression you get will evaluate to K (a number also given as input). Putting an empty string ("") between two numbers means, that the numbers are joined to form a new number (e.g. 1 "" 2 = 12)

Order of integers given as input needs to remain the same.

Input:
1. String of positive integers
2. Target K

Output:
All possible strings that evaluate to K

e.g.
If the input string is "222", and your target (K) is "24", two possible answers are:

1. 22+2 (Here, we put the "" operator in between first two 2s and then put a plus between the last two)
2. 2+22 (Here, we put the plus operator between first two 2s and then put the "" operator between the last two)

Realize that precedence of the operators matters. In higher to lower precedence:
1. Join ("")
2. Multiplication (*)
3. Addition (+)
"""
