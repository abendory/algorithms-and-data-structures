import java.util.*;

public class Recursion {
  // BEST PATH
  public static int bestPath(int[][] array) {
    return bestPath(array, 0, 0);
  }

  private static int bestPath(int[][] array, int i, int j) {
    int m = array.length;
    int n = array[0].length;

    if (i == m-1 && j == n-1)
      return array[i][j];

    if (i == m-1)
      return array[i][j] + bestPath(array, i, j + 1);

    if (j == n-1)
      return array[i][j] + bestPath(array, i + 1, j);

    int down = array[i][j] + bestPath(array, i + 1, j);
    int right = array[i][j] + bestPath(array, i, j + 1);
    return Math.max(down, right);

  }

  // SUBSETS
  public static void printSubsets(Object[] set) {
    Object[] subset = new Object[set.length];
    printSubsets(set, 0, subset, 0);
  }

  private static void printSubsets(Object[] set, int read, Object[] subset, int write) {
    if (read == set.length) {
      System.out.print("{");
      for (int i = 0; i < write; ++i) {
        System.out.print(subset[i]);
        if (i + 1 != write)
          System.out.print(", ");
      }
      System.out.println("}");
      return;
    }
    printSubsets(set, read + 1, subset, write); // Don't select
    subset[write] = set[read]; // Select
    printSubsets(set, read + 1, subset, write + 1);
  }

  //PERMUTATIONS
   private static void swap(Object[] array, int i, int j) {
     Object temp = array[i];
     array[i] = array[j];
     array[j] = temp;
   }

  public static void printPermutations(Object[] array) {
    printPermutations(array, 0);
  }

  private static void printPermutations(Object[] array, int i) {
    if (i == array.length) {
      for (Object obj : array)
        System.out.print(" " + obj);
      System.out.println();
      return;
    }

    for (int j = i; j < array.length; ++j) {
      swap(array, i, j);
      printPermutations(array, i + 1);
      swap(array, i, j);
    }
  }

  // DIAMETER OF A TREE
  public static class TreeNode {
    int distanceFromFather;
    ArrayList<TreeNode> sons;
  }

  private static class DiameterReturnValue {
    int diameter, distanceToMostDistantLeaf;
  }

  public static int diameter(TreeNode tree) {
    if (tree == null)
     return 1;

    return diameterRecursion(tree).diameter;
  }

  private static DiameterReturnValue diameterRecursion(TreeNode tree) { DiameterReturnValue returnValue = new DiameterReturnValue();

    if (tree.sons == null) {
      returnValue.distanceToMostDistantLeaf = tree.distanceFromFather;
      returnValue.diameter = 0;
      return returnValue;
    }

    returnValue.diameter = 1;

    int total_max_distance, total_2nd_max_distance;
    total_max_distance = total_2nd_max_distance = 0;

    for (TreeNode son : tree.sons) {
      DiameterReturnValue r = diameterRecursion(son);
      returnValue.diameter = Math.max(returnValue.diameter, r.diameter);

      if (r.distanceToMostDistantLeaf > total_max_distance) {
        total_2nd_max_distance = total_max_distance;
        total_max_distance = r.distanceToMostDistantLeaf;
      } else if (r.distanceToMostDistantLeaf > total_2nd_max_distance) {
        total_2nd_max_distance = r.distanceToMostDistantLeaf;
      }
    }
    returnValue.distanceToMostDistantLeaf = total_max_distance + tree.distanceFromFather;
    returnValue.diameter = Math.max(returnValue.diameter, total_max_distance + total_2nd_max_distance);
    return returnValue;
  }


  public static void main(String[] args) {
    //Object[] set = {1, 3, 5, 2, 6, 8, 3};
    //printSubsets(set);

    int[][] array = new int[][]{
      { 4, 2, 1, 8, 9 },
      { 1, 2, 3, 3, 1 },
      { 4, 9, 0, 9, 2 },
      { 5, 8, 5, 4, 3 }
    };
    System.out.println("The best path is: " + bestPath(array));
  }
}
