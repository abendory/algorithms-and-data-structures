import sys


def palindrome(string):
    i = 0
    j = len(string)-1
    while string[i] == string[j]:
        if i == j:
            return True
        i += 1
        j -= 1
    return False


def main(string):
    print(palindrome(string))


if __name__ == "__main__":
    main(sys.argv[1])
