#!/usr/bin/env python3

# Matrix Recursion (best path)

# Assumptions: MxN matrix, weights are in a 0,0 indexed matrix
# There are 4 cases:
# BP = best path
# case 1: BP(i, j) = W[i,j] if i = M-1 and j = N-1
# case 2: BP(i, j) = W[i,j] + BP(i, j+1) if i = M-1 and j < N-1  # edge case
# case 3: BP(i, j) = W[i,j] + BP(i+1, j) if i < M-1 and j = N-1  # edge case
# case 4: BP(i, j) = W[i,j] + max{BP(i+1, j), BP(i, j+1)} if i < M-1 and j < N-1

# +---+---+---+---+
# | 5 | 3 | 2 | 4 | i
# +---+---+---+---+ |
# | 7 | 8 | 5 | 3 | |
# +---+---+---+---+ |
# | 0 | 9 | 6 | 4 | |
# +---+---+---+---+
# | 3 | 5 | 7 | 3 | N
# +---+---+---+---+
#   j ------->  M

# Recursive
def best_path(array):
    return _best_path(array, 0, 0)

def _best_path(array, i, j):
    # store up the dimensions of the matrix
    m = len(array[0])
    n = len(array)

    # base case
    if (i == n-1 and j == m-1):
        return array[i][j]

    # edge case
    if (i == n-1):
        return array[i][j] + _best_path(array, i, j+1)

    # edge case
    if (j == m-1):
        return array[i][j] + _best_path(array, i+1, j)

    return array[i][j] + max(_best_path(array, i, j+1),
                             _best_path(array, i+1, j))


# Using memoization
def best_path_cached(array):
    cache = [[-1 for x in range(len(array[0]))] for y in range(len(array))]
    return _best_path_cached(array, 0, 0, cache)

def _best_path_cached(array, i, j, cache):
    # store up the dimensions of the matrix
    m = len(array[0])
    n = len(array)

    # check the cache
    if cache[i][j] > -1:
        return cache[i][j]

    # base case
    if (i == n-1 and j == m-1):
        cache[i][j] = array[i][j]

    # edge case
    elif (i == n-1):
        cache[i][j] = array[i][j] + _best_path_cached(array, i, j+1, cache)

    # edge case
    elif (j == m-1):
        cache[i][j] = array[i][j] + _best_path_cached(array, i+1, j, cache)

    else:
        cache[i][j] = array[i][j] + max(_best_path_cached(array, i, j+1, cache)                         , _best_path_cached(array, i+1, j, cache))
    return cache[i][j]

if __name__ == '__main__':
    # create a randomly weighted 2d 5x7 list
    # import random
    # array = [[random.randint(1, 9) for x in range(5)] for x in range(7)]
    array = [[1,3,2],[2,6,4],[6,2,3],[1,2,4]]

    # print the list
    print(array)
    print()

    print("The best path is", best_path(array))
    print("The best path (cached) is", best_path_cached(array))
