"""
Wildcard
Input: 10?
Output: 101 100

i.e. ? behaves like a wild-card.

Input: 1?0?
Output: 1000 1001 1100 1101

Please write a program that takes given strings as input and produces the suggested output.

Suggested time: 20 minutes"""

def wild_card(string):
    result = []
    getWildcard("", string, result)

def getWildcard(prefix, string, res):
    if len(string) > 0:
        for i, v in enumerate(string):
            if v == '?':
                res.append(prefix + string[:i] + '0')
                getWildcard(prefix + string[:i] + '0', string[i+1:], res)
                getWildcard(prefix + string[:i] + '1', string[i+1:], res)
    return res



print(wild_card('1?0?'))  # 1000 1100 1001 1101
