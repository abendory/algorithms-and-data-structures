#!/usr/bin/env python3

# Given array of k ascii strings of length n each, return all groups of strings that are anagrams.


def anagrams1(strings):
    """solution 1  O(knlogn) time O(kn) space"""
    hashmap = {}
    for string in strings:
        sorted_string = "".join(sorted(string))
        if sorted_string in hashmap:
            hashmap[sorted_string].append(string)
        else:
            hashmap[sorted_string] = [string]
    return hashmap


def anagrams2(strings):
    """do the same thing as above except allocate an array of all ascii chars and check against that array for each char in each string rather than sorting"""
    ascii_chars = [chr(i) for i in range(127)]
    hashmap = {}
    for string in strings:
        for char in string:
            if char in ascii_chars:
        if sorted_string in hashmap:
            hashmap[sorted_string].append(string)
        else:
            hashmap[sorted_string] = [string]
    return hashmap


def anagrams3(strings):
    """prime number solution
        if each ascii char is assigned to a prime number using a map and then take the first string and multiply each char by its prime number. that makes a unique representation of the string"""

strings = ['abc', 'def', 'cba', 'efd', 'fed', 'bca', 'gef', 'fge', 'zed']
print(anagrams1(strings))
