#!/usr/bin/env python3

def subsets(s):
    subset = []
    return _subsets("", s, subset)

def _subsets(prefix, s, subset):
    if len(s) > 0:
        subset += [prefix + s[0]]
        _subsets(prefix + s[0], s[1:], subset)
        _subsets(prefix, s[1:], subset)
    return subset

def subsets_binary(s):
    number_of_elements = len(s)
    result = []
    for num in range(1, 1 << number_of_elements):
        lis = []
        for el in range(number_of_elements):
            if (num & (1 << el)):
               lis.append(s[el])
        result.append("".join(lis))
    return result


s = "abc"
print(subsets(s))
print(subsets_binary(s))
