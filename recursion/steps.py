def countways_r(n):
    if n < 0:
        return 0
    elif n == 0:
        return 1
    else:
        return countways(n-1) + countways(n-2) + countways(n-3)

def countways_m(n):
    memo = [-1] * (n+1)
    return _countways_m(n, memo)

def _countways_m(n, memo):
    if n < 0:
        return 0
    elif n == 0:
        return 1
    elif memo[n] > -1:
        return memo[n]
    else:
        return _countways_m(n-1, memo) + _countways_m(n-2, memo) + \
            _countways_m(n-3, memo)
    return memo[n]


print(countways_m(30))
