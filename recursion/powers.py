def pow(base, power):
    if power > 0:
        return _pow(base, power)
    elif power < 0:
        return 1 / _pow(base, abs(power))

def _pow(base, power):
    if power == 0 or base == 1:
        return 1
    return base * _pow(base, power-1)


print(pow(-2.5, -3))
