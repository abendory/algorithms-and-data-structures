def numberOfPaths(a):
    return num_paths(a, 0, 0, 0)

def num_paths(array, i, j, total):
    # store up the dimensions of the matrix
    c = len(array[0])
    r = len(array)
    # base cases
    if (array[i][j] == 0):
        return total

    if (i == r-1 and j == c-1):
        return array[i][j] + total

    # edge case
    if (i == r-1):
        return total + num_paths(array, i, j+1, total)

    # edge case
    if (j == c-1):
        return total + num_paths(array, i+1, j, total)

    total += num_paths(array, i, j+1, total) + num_paths(array, i+1, j, total)
    return total

a = [[1, 1, 1, 1], [1, 1, 1, 1], [1, 1, 1, 1]]

print(numberOfPaths(a))
