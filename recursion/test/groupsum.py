# CODING BAT RECURSION II

"""Given an array of ints, is it possible to choose a group of some of the ints, such that the group sums to the given target? This is a classic backtracking recursion problem. Once you understand the recursive backtracking strategy in this problem, you can use the same pattern for many problems to search a space of choices. Rather than looking at the whole array, our convention is to consider the part of the array starting at index start and continuing to the end of the array. The caller can specify the whole array simply by passing start as 0. No loops are needed -- the recursive calls progress down the array.

groupSum(0, {2, 4, 8}, 10) → true
groupSum(0, {2, 4, 8}, 14) → true
groupSum(0, {2, 4, 8}, 9) → false
"""


def groupSum(start, nums, target):
    if (start >= len(nums)):
        return target == 0
    return groupSum(start + 1, nums, target-nums[start]) or \
        groupSum(start + 1, nums, target)


def groupSumMe(pre, a, subset, k):
    if len(a) > 0:
        subset = pre + a[0]
        if subset == k:
            return True
        return (_groupSumMe(a[0] + pre, a[1:], subset, k) or
                _groupSumMe(pre, a[1:], subset, k))

    return False


"""Given an array of ints, is it possible to choose a group of some of the ints, beginning at the start index, such that the group sums to the given target? However, with the additional constraint that all 6's must be chosen. (No loops needed.)

groupSum6(0, {5, 6, 2}, 8) → true
groupSum6(0, {5, 6, 2}, 9) → false
groupSum6(0, {5, 6, 2}, 7) → false"""


def groupSum6(start, nums, target):
    if start >= len(nums):
        return target == 0
    if nums[start] == 6:
        return (start+1, nums, target-nums[start])
    return groupSum6(start+1, nums, target - nums[start]) or \
        groupSum6(start+1, nums, target)


"""Given an array of ints, is it possible to choose a group of some of the ints, such that the group sums to the given target with this additional constraint: If a value in the array is chosen to be in the group, the value immediately following it in the array must not be chosen. (No loops needed.)

groupNoAdj(0, {2, 5, 10, 4}, 12) → true
groupNoAdj(0, {2, 5, 10, 4}, 14) → false
groupNoAdj(0, {2, 5, 10, 4}, 7) → false"""


def groupNoAdj(start, nums, target):
    if start >= len(nums):
        return target == 0
    return groupNoAdj(start+2, nums, target-nums[start]) or \
        groupNoAdj(start+1, nums, target)


"""Given an array of ints, is it possible to choose a group of some of the ints, such that the group sums to the given target with these additional constraints: all multiples of 5 in the array must be included in the group. If the value immediately following a multiple of 5 is 1, it must not be chosen. (No loops needed.)

groupSum5(0, {2, 5, 10, 4}, 19) → true
groupSum5(0, {2, 5, 10, 4}, 17) → true
groupSum5(0, {2, 5, 10, 4}, 12) → false"""


def groupSum5(start, nums, target):
    if start >= len(nums):
        return target == 0
    if nums[start] % 5 == 0:
        if start+1 < len(nums) and nums[start+1] == 1:
            return groupSum5(start+2, nums, target-nums[start])
        return groupSum5(start+1, nums, target-nums[start])
    return groupSum5(start+1, nums, target) or \
        groupSum5(start+1, nums, target-nums[start])


"""Given an array of ints, is it possible to choose a group of some of the ints, such that the group sums to the given target, with this additional constraint: if there are numbers in the array that are adjacent and the identical value, they must either all be chosen, or none of them chosen. For example, with the array {1, 2, 2, 2, 5, 2}, either all three 2's in the middle must be chosen or not, all as a group. (one loop can be used to find the extent of the identical values).

groupSumClump(0, {2, 4, 8}, 10) → true
groupSumClump(0, {1, 2, 4, 8, 1}, 14) → true
groupSumClump(0, {2, 4, 4, 8}, 14) → false"""


def groupSumClump(start, nums, target):
    if start >= len(nums):
        return target == 0
    count = 1
    for i in range(start+1, len(nums)):
        if nums[i] == nums[start]:
            count += 1
            total += nums[i]
    return groupSumClump(start+count, nums, target-total) or \
        groupSumClump(start+count, nums, target)


"""Given an array of ints, is it possible to divide the ints into two groups, so that the sums of the two groups are the same. Every int must be in one group or the other. Write a recursive helper method that takes whatever arguments you like, and make the initial call to your recursive helper from splitArray(). (No loops needed.)

splitArray({2, 2}) → true
splitArray({2, 3}) → false
splitArray({5, 2, 3}) → true"""

def splitArray(nums):
    return _splitArray(0, nums, 0, 0)

def _splitArray(start, nums, group1, group2):
    if start >= len(nums):
        return group1 == group2
    return _splitArray(start+1, nums, group1+nums[start], group2) or \
        _splitArray(start+1, nums, group1, group2+nums[start])


"""Given an array of ints, is it possible to divide the ints into two groups, so that the sum of one group is a multiple of 10, and the sum of the other group is odd. Every int must be in one group or the other. Write a recursive helper method that takes whatever arguments you like, and make the initial call to your recursive helper from splitOdd10(). (No loops needed.)

splitOdd10({5, 5, 5}) → true
splitOdd10({5, 5, 6}) → false
splitOdd10({5, 5, 6, 1}) → true"""

def splitOdd10(nums):
    return _splitOdd10(0, nums, 0, 0)

def _splitOdd10(start, nums, g1, g2):
    if start >= len(nums):
        return g1 % 10 == 0 and g2 % 2 == 1
    return _splitOdd10(start+1, nums, g1-nums[start], g2) or \
        _splitOdd10(start+1, nums, g1, g2-nums[start])


"""Given an array of ints, is it possible to divide the ints into two groups, so that the sum of the two groups is the same, with these constraints: all the values that are multiple of 5 must be in one group, and all the values that are a multiple of 3 (and not a multiple of 5) must be in the other. (No loops needed.)

split53({1,1}) → true
split53({1, 1, 1}) → false
split53({2, 4, 2}) → true"""


def split53(nums):
    return _split53(0, nums, 0, 0)

def _split53(start, nums, g1, g2):
    if start >= len(nums):
        return g1 == g2
    if nums[start] % 5 == 0:
        return _split53(start+1, nums, g1-nums[start], g2)
    if nums[start] % 3 == 0:
        return _split53(start+1, nums, g1, g2-nums[start])
    return _split53(start+1, nums, g1-nums[start], g2) or \
        _split53(start+1, nums, g1, g2-nums[start])

"""We want to make a row of bricks that is goal inches long. We have a number of small bricks (1 inch each) and big bricks (5 inches each). Return true if it is possible to make the goal by choosing from the given bricks. This is a little harder than it looks and can be done without any loops. See also: Introduction to MakeBricks

make_bricks(3, 1, 8) → true
make_bricks(3, 1, 9) → false
make_bricks(3, 2, 10) → true"""

def make_bricks(small, big, goal):
    if goal < 0 or small < 0 or big < 0:
        return False
    if goal == 0:
        return True

    return make_bricks(small, big-1, goal-5) or \
        make_bricks(small-1, big, goal-1)



"""
We want make a package of goal kilos of chocolate. We have small bars (1 kilo each) and big bars (5 kilos each). Return the number of small bars to use, assuming we always use big bars before small bars. Return -1 if it can't be done.

make_chocolate(4, 1, 9) → 4
make_chocolate(4, 1, 10) → -1
make_chocolate(4, 1, 7) → 2"""


def make_chocolate(small, big, goal, total):
    return _make_chocolate(small, big, goal, total)


def _make_chocolate(small, big, goal, total):
    if goal < 0 or small < 0 or big < 0:
        return -1
    if goal == 0:
        return total

    return _make_chocolate(small, big-1, goal-5) or \
        _make_chocolate(small-1, big, goal-1)

# main
if __name__ == '__main__':
    array = [2, 6, 4, 8]

    print(make_bricks(3, 1, 8))    # true
    print(make_bricks(3, 1, 9))    # false
    print(make_bricks(3, 2, 10))   # true
    print(make_bricks(2, 10, 16))  # true
