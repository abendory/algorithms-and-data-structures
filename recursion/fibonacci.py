def fib(n):
    if n in (0, 1): return n
    return (fib(n-1) + fib(n-2))

def fib_cache(n):
    def _fib_cache(n, memo):
        if memo[n] < 0:
            memo[n] = _fib_cache(n-1, memo) + \
                      _fib_cache(n-2, memo)
        return memo[n]
    memo = [0, 1] + [-1] * (n-1)
    return _fib_cache(n, memo)


print(fib(10))
print(fib_cache(40))

# import timeit
# timeit.repeat("for n in range(10): fib(n)", "from __main__ import fib", number = 100)
# timeit.repeat("for n in range(10): fib_cache(n)", "from __main__ import fib_cache", number = 100)
