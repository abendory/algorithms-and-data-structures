#include <stdio.h>

void printPermutationsRecursion(int n, int *array, int i) {
    if (i == n) {
        for(int j = 0; j < n; ++j)
            printf("%d ", array[j]);
        printf("\n");
        return;
    }

    for(int j = i; j < n; ++j) {
        int tmp = array[j];
        array[j] = array[i];
        array[i] = tmp;
        printPermutationsRecursion(n, array, i+1);
        array[i] = array[j];
        array[j] = tmp;
    }

}


int main() {
    int array[] = {1, 2, 3};
    printPermutationsRecursion(3, array, 3);

    return 0;
}
