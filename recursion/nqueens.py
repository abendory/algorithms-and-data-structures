
def print_row(length):
    for i in range(length):
        print('+-', end="")
    print('+')

def print_cell(array, length):
    for i in range(length):
        if i == array[0]:
            print('|*', end="")
        else:
            print('| ', end="")
    print("|")

def print_matrix(array):
    """Prints the queens n-matrix"""
    array = list(map(int, array))
    length = len(array)
    for i in range(length):
        print_row(length)
        print_cell(array[i:], length)
    print_row(length)
    print()

def is_valid(row):
    for i in range(len(row)-1):
        if row[i] == row[i+1]:  # check row
            return False
        if row[i] == row[i+1]+1 or row[i] == row[i+1]-1:  # check diagonal
            return False
    return True

def n_queens(row):
    def _permute(row, start, end, permutations):
        if (start == end):
            if is_valid(row):
                string = ''.join(map(str, row))
                permutations.append(string)
        else:
            for i in range(start, end+1):
                row[start], row[i] = row[i], row[start]  # swap
                _permute(row, start+1, end, permutations)
                row[start], row[i] = row[i], row[start]  # swap
        return permutations
    permutations = []
    return _permute(row, 0, len(row)-1, permutations)

rows = list(range(5))
permutations = n_queens(rows)
for index, valid_solution in enumerate(permutations):
    print('Solution', index+1)
    print_matrix(valid_solution)

