#!/usr/bin/env python3

# Generate unique permutations of a list

class Solution:
    # @param A : list of integers
    # @return a list of list of integers
    def permute(self, nums):
        if not nums:
            return [[]]
        else:
            sign = 1
            new_nums = []
            nextval = [nums.pop()]
            for num in self.permute(nums):
                length = len(num)
                try:
                    maxinsert = num.index(nextval[0])
                except ValueError:
                    maxinsert = length
                if sign == 1:
                    new_nums += [num[:i] + nextval + num[i:] for i in range(length, -1, -1)
                                  if i <= maxinsert]
                else:
                    new_nums += [num[:i] + nextval + num[i:] for i in range(length + 1)
                                  if i <= maxinsert]
                sign *= -1

            return new_nums


nums = [1,1,2]
s = Solution()
perms = s.permute(nums)
print(perms)
