"""
Palindromic Decomposition

A “Palindromic Decomposition” of string S, is a decomposition of the string
into substrings, such that all those substrings are valid palindromes.
A single character is considered a valid palindrome for this problem.
Print out all possible palindromic decompositions of a given string.

e.g.
Input: abracadabra

Output:
a|b|r|a|c|a|d|a|b|r|a|
a|b|r|a|c|ada|b|r|a|
a|b|r|aca|d|a|b|r|a|

Solution: http://www.programcreek.com/2013/03/leetcode-palindrome-partitioning-java/
(Hopefully, this solution will make you appreciate Recursion and eventually, Dynamic Programming :-))

(Suggested time: 45 minutes)"""

import logging
logging.basicConfig(level=logging.DEBUG, format='%(levelname)s %(message)s')


def is_palindrome(s):
    for i in range(len(s)):
        if s[i] != s[len(s)-1-i]:
            return False
    return True


def dfs(s, stringlist, res):
    logging.debug("Enter dfs: s: " + s + "\t\tstringlist:" + str(stringlist))
    if len(s) == 0:
        res.append(stringlist)
    for i in range(1, len(s)+1):
        if is_palindrome(s[:i]):
            dfs(s[i:], stringlist+[s[:i]], res)


def palindromicDecomposition(s):
    res = []
    dfs(s, [], res)
    for i, array in enumerate(res):
        res[i] = "|".join(array)
    return res


if __name__ == '__main__':
    string = "abracadabra"
    print(palindromicDecomposition(string))
