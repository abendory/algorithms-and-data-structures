

def move(discs, A, B, C):
    if discs >= 1:
        move(discs-1, A, C, B) # move remaining discs from A -> B
        print("move", A, "to", C) # move largest disc from A -> C
        move(discs-1, B, A, C) # move remaining discs from B -> C

move(5, 'A', 'B', 'C') # move 1 disc from A to C
print()

