#!/usr/bin/env python3

# Permutations of a string

def permute(word):
    def permute(word, start, end, permutations):
        if (start == end):
            string = "".join(word)
            permutations.append(string)
        else:
            for i in range(start, end+1):
                word[start], word[i] = word[i], word[start]  # swap
                permute(word, start+1, end, permutations)
                word[start], word[i] = word[i], word[start]  # swap
        return permutations
    word = list(word)
    permutations = []
    return permute(word, 0, len(word)-1, permutations)

def permute_nums(n):
    def permute(n, start, end, permutations):
        if (start == end):
            permutations.append(n)
        else:
            for i in range(start, end+1):
                n[start], n[i] = n[i], n[start]  # swap
                permute(n, start+1, end, permutations)
                n[start], n[i] = n[i], n[start]  # swap
        return permutations
    permutations = []
    return permute(n, 0, len(n)-1, permutations)

def all_perms(elements):
    if len(elements) <= 1:
        yield elements
    else:
        for perm in all_perms(elements[1:]):
            for i in range(len(elements)):
                yield perm[:i] + elements[0:1] + perm[i:]


if __name__ == '__main__':
    #print(permute("hello"))
    print(permute_nums([1,2,3]))
    # print()
    # print(list(all_perms("hello")))
