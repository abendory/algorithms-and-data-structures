INF = float('inf')
def maxSubArray(A):
    if len(A) == 1: return A[0]
        
    # if min array
    min_max = -INF

    current_max = max_overall = 0
    for element in A:
        current_max = max(-INF, current_max + element)
        max_overall = max(max_overall, current_max)
        if element > min_max:
            min_max = element

    if min_max < 0:
        return min_max
    else:
        return max_overall

print(maxSubArray([-163, -20]))
