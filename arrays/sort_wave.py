class Solution:
    # @param A : list of integers
    # @return a list of integers
    def wave_O_nlogn(self, A):
        n = len(A)
        A.sort()

        for i in range(0,n-1,2):
            A[i], A[i+1] = A[i+1], A[i]

    def wave_O_n(self, A):
        n = len(A)
        for i in range(0, n, 2):

            # If current even element is smaller than previous
            if (i> 0 and A[i] < A[i-1]):
                A[i],A[i-1]=A[i-1],A[i]

            # If current even element is smaller than next
            if (i < n-1 and A[i] < A[i+1]):
                A[i],A[i+1]=A[i+1],A[i]

s = Solution()
A = [5,1,3,2,4]
s.wave_O_n(A)
print(A)
