class Solution:
    # @param X : list of integers
    # @param Y : list of integers
    # Points are represented by (X[i], Y[i])
    # @return an integer
    def coverPoints(self, X, Y):
        if len(X) != len(Y):
            return -1

        count = 0

        # loop through each coordinate
        for i in range(len(X)-1):
            # calc distance
            x_distance = abs(X[i] - X[i+1])
            y_distance = abs(Y[i] - Y[i+1])
            count += max(x_distance, y_distance)

        return count

