from pprint import pprint as pp
class Solution:
    # @param A : integer
    # @return a list of list of integers
    def prettyPrint(self, A):
        rows = cols = A+A-1
        m = [[None for j in range(cols)] for i in range(rows)]

        row, col = 0, 0

        while row < rows and col < cols:

            # add first row
            for i in range(col, cols):
                m[row][i] = A
            row += 1

            # add last col
            for i in range(row, rows):
                m[i][cols-1] = A
            cols -= 1

            # add last row
            if row < rows:
                for i in range(cols-1, col-1, -1):
                    m[rows-1][i] = A
                rows -= 1

            # add first col
            if col < cols:
                for i in range(rows-1, row-1, -1):
                    m[i][col] = A
                col += 1

            A -= 1

        pp(m)

s = Solution()
s.prettyPrint(7)
