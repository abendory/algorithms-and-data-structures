class Solution:
    # @param A : integer
    # @return a list of integers
    def primesum(self, A):  # using an O(n^2) attempt after trying Sieve and hitting memory limit
        i = 2
        j = A-1
        for i in range(2, A):
            if not self.is_prime(i):
                continue
            else:
                j = A-1
                while i <= j:
                    if self.is_prime(j) and i + j == A:
                        return [i, j]
                    else:
                        j -= 1

    def is_prime(self, A):
        if A in (2, 3, 5): return True
        if A <= 1 or A % 2 == 0: return False
        sqrt = int(A ** 0.5)
        for i in range(5, sqrt+1, 2):
            if A % i == 0:
                return False
        return True

    def prime_sieve(self, n):
        # returns all primes smaller than n
        sieve = [True] * n
        sieve[:2] = [False, False]  # 0 and 1 are not primes
        primes = []
        for prime, is_prime in enumerate(sieve):
            if not is_prime:
                continue
            primes.append(prime)
            for not_prime in xrange(prime*prime, n, prime):
                sieve[not_prime] = False
        return primes

    def primesum2(self, value):
        primes = prime_sieve(value)
        lo = 0
        hi = len(primes) - 1
        while lo <= hi:
            prime_sum = primes[lo] + primes[hi]
            if prime_sum < value:
                lo += 1
            else:
                if prime_sum == value:
                    return primes[lo] + primes[hi]
                hi -= 1

s = Solution()
#for i in range(100):
print(s.primesum(4))
