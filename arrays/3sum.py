# Find 3 numbers in an array that sum up to zero

class Solution:
    # @param A : list of integers
    # @return a list of list of integers
    def threeSum(self, a):
        n = len(a)
        if n < 3:
            return []

        a.sort()
        res = []
        for i in range(n-2):
            if i > 0 and i < n-1 and a[i] == a[i-1]:
                continue
            target = -a[i]
            s = i + 1
            e = n - 1
            while s < e:
                if a[i]+ a[s] + a[s] > 0 or a[i] + a[e] + a[e] < 0:
                    break
                twosum = a[s] + a[e]
                if twosum == target:
                    res.append([a[i], a[s], a[e]])
                    lv = a[s]
                    while s < e and lv == a[s]:
                        s += 1
                    rv = a[e]
                    while s < e and rv == a[e]:
                        e -= 1
                elif twosum < target:
                    s += 1
                else: #twosum > target:
                    e -= 1
        return res

s = Solution()
a = [1, -4, 0, 0, 5, -5, 1, 0, -2, 4, -4, 1, -1, -4, 3, 4, -1, -1, -3]
#   [-5, -4, -4, -4, -3, -2, -1, -1, -1, 0, 0, 0, 1, 1, 1, 3, 4, 4, 5]
#a = [ -4, 2, -1, 1, -4, 2, -5, -3, 2 ]
#a = [ -4, 2, -1 ]
print(s.threeSum(a))

# [-5  0 5] [-5  1 4] [-4 -1 5] [-4 0 4]
# [-4  1 3] [-3 -2 5] [-3 -1 4] [-3 0 3]
# [-2 -1 3] [-2  1 1] [-1  0 1] [ 0 0 0]
