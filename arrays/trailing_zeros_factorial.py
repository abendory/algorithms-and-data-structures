class Solution:
    # @param A : integer
    # @return an integer
    def trailingZeroes(self, A):
        zeros = 0
        while A:
            zeros += A // 5
            A //= 5
        return zeros

    def trailingZeroes2(self, A):
        total_factors_of_5 = 0
        n = 5
        i = 2
        while factors_of_5:
            factors_of_5 = A // n
            total_factors_of_5 += factors_of_5
            n = 5 ** i
            i += 1

        return total_factors_of_5

s = Solution()
A = 1000
print(s.trailingZeroes(A))
