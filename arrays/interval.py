# Definition for an interval.
class Interval:
    def __init__(self, s=0, e=0):
        self.start = s
        self.end = e
    def __repr__(self):
        return str([self.start, self.end])


class Solution:
    # @param intervals, a list of Intervals
    # @param new_interval, a Interval
    # @return a list of Interval
    def insert(self, intervals, new_interval):
        # 1) if no intervals just return the new interval
        if not intervals:
            return [new_interval]

        s, e = 0, -1

        if new_interval.start > new_interval.end:
            new_interval.start, new_interval.end = new_interval.end, new_interval.start

        # 2) new_interval preceding array
        if new_interval.end < intervals[s].start:
            intervals.insert(0, new_interval)
            return intervals
            
        # 3) new_interval succeeding array
        if new_interval.start > intervals[e].end:
            intervals.append(new_interval)
            return intervals
    
        # 4) new_interval encompasses all other intervals in array.
        if new_interval.start < intervals[s].start and new_interval.end > intervals[e].end:
            return [new_interval]
                
        res = []
        for i, interval in enumerate(intervals):
            
            # 5) new_interval in between 2 intervals in the array w/o overlap.
            if new_interval.start > interval.end and new_interval.end < intervals[i+1].start:
                intervals.insert(i+1, new_interval)
                return intervals
            
            # 6) new_interval overlaps 
            if interval.end < new_interval.start:
                res.append(interval)
            elif interval.start > new_interval.end:
                res.append(new_interval)
                new_interval = interval
            elif interval.end >= new_interval.start or interval.start <= new_interval.end:
                new_interval = Interval(min(interval.start, new_interval.start), max(new_interval.end, interval.end))

        res.append(new_interval)

        return res
            
# Test
s = Solution()
a = Interval(2,2)
b = Interval(3,5)
c = Interval(6,7)
d = Interval(8,10)
e = Interval(14,16)
print(s.insert([], Interval(1,1)))
print(s.insert([a,b,c,d,e], Interval(1,1)))
print(s.insert([a,b,c,d,e], Interval(17,18)))
print(s.insert([a,b,c,d,e], Interval(1,17)))
print(s.insert([a,b,c,d,e], Interval(12,13)))
print(s.insert([a,b,c,d,e], Interval(4,9)))
