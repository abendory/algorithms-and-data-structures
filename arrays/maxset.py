class Solution:    
    def maxset(self, A):
        current_max = overall_max = result_index = 0
        current_result = []
        overall_result = []
        
        for element in A:
            if element >= 0:
                current_result.append(element)
                current_max += element
            else:
                current_result = []
                current_max = 0

            if current_max > overall_max:
                overall_max = current_max
                overall_result = current_result
            if current_max == overall_max:
                if len(current_result) > len(overall_result):
                    overall_result = current_result

        return overall_result

s = Solution()
print(s.maxset([1,2,5,-7,1,2,3]))
print(s.maxset([1]))
print(s.maxset([1,2,5,-7,1,1,1,5]))
print(s.maxset([1,2,1,5,-7,1,1,2,5]))

