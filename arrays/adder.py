"""Given a non-negative number represented as an array of digits,
add 1 to the number ( increment the number represented by the digits ).
The digits are stored such that the most significant digit is at the head of the list."""


class Solution:
    # @param A : list of integers
    # @return a list of integers
    def plusOne(self, A):
        n = len(A)
        if n == 1 and A[0] in range(0,9):
            return [A[0]+1]
        carry = 1
        result = [None] * n
        newresult = []
        for i in reversed(range(n)):
            value = A[i] + carry
            result[i] = value % 10
            carry = value // 10
        if carry == 1:
            newresult.append(1)
            newresult.extend(result)

        if newresult:
            i = 0
            while newresult[i] == 0:
                newresult.pop(0)
            return newresult
        else:
            i = 0
            while result[i] == 0:
                result.pop(0)
            return result


s = Solution()
a = [ 2, 5, 6, 8, 6, 1, 2, 4, 5 ]
print(s.plusOne(a))
