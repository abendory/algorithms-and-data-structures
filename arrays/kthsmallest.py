import random
class Solution:

    def kthsmallest(self, A, k):
        if not A or k < 0:
            return

        n = len(A)

        return self._select(A, 0, n-1, k-1)

    def _select(self, A, left, right, k):
        while True:
            pivot = random.randint(left, right)
            pivot_new_index = self.partition(A, left, right, pivot)
            pivot_distance = pivot_new_index - left
            if pivot_distance == k:
                return A[pivot_new_index]
            elif k < pivot_distance:
                right = pivot_new_index - 1
            else:
                k -= pivot_distance + 1
                left = pivot_new_index + 1

    def partition(self, A, left, right, pivot):
        pivot_value = A[pivot]
        A[pivot], A[right] = A[right], A[pivot]
        index = left
        for i in range(left, right):
            if A[i] < pivot_value:
                A[index], A[i] = A[i], A[index]
                index += 1
        A[right], A[index] = A[index], A[right]
        return index

# Tests
arr = [8,  16, 80, 55, 32,  8, 38, 40, 65, 18, 15,
       45, 50, 38, 54, 52, 23, 74, 81, 42, 28, 16,
       66, 35, 91, 36, 44,  9, 85, 58, 59, 49, 75, 20,
       87, 60, 17, 11, 39, 62, 20, 17, 46, 26, 81, 92]
v = 9

arr = [47]
v = 1

arr = [47, 2]
v = 2

arr = [60, 94, 63, 3,  86, 40, 93, 36, 56, 48, 17, 10, 23,
       43, 77,  1, 1,  93, 79,  4, 10, 47,  1, 99, 91, 53, 99,
       18, 52, 61, 84, 10, 13, 52,  3,  9, 78, 16, 7, 62]
v = 21
# [1, 1, 1, 3, 3, 4, 7, 9, 10, 10, 10, 13, 16, 17, 18, 23, 36, 40, 43, 47, 48, 52, 52, 53, 56, 60, 61, 62, 63, 77, 78, 79, 84, 86, 91, 93, 93, 94, 99, 99]

s = Solution()
print(s.kthsmallest(arr, v))

