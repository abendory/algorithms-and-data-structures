class Solution:
    # @param A : list of integers
    # @return an integer
    def countInversions_naive(self, A):
        count = 0
        for i in range(len(A)-1):
            for j in range(i+1, len(A)):
                if A[i] > A[j] and i < j:
                    count += 1

        return count

    def countInversions(self, A):
        left, right = 0, len(A)-1
        array = [None] * len(A)
        return self.mergesort(A, array, left, right)

    def mergesort(self, a, merged, left, right):
        inv_count = 0

        if right > left:
            mid = left + (right-left) // 2

            inv_count = self.mergesort(a, merged, left, mid)
            inv_count += self.mergesort(a, merged, mid+1, right)

            inv_count += self.merge(a, merged, left, mid+1, right)

        return inv_count

    def merge(self, a, m, left, mid, right):
        inv_count = 0
        i = left
        j = mid
        k = left
        while i <= mid - 1 and j <= right:
            if a[i] <= a[j]:
                m[k] = a[i]
                i += 1
            else:
                m[k] = a[j]
                j += 1
                inv_count += mid - i
            k += 1

        while i <= mid - 1:
            m[k] = a[i]
            i += 1
            k += 1

        while j <= right:
            m[k] = a[j]
            j += 1
            k += 1

        for i in range(left, right+1):
            a[i] = m[i]

        return inv_count


s = Solution()
a = [59, 29]
print(s.countInversions(a))
