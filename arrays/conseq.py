"""
  Given an unsorted array of integers, find the length of the longest consecutive elements sequence.

  Example:
  Given [100, 4, 200, 1, 3, 2],
  The longest consecutive elements sequence is [1, 2, 3, 4]. Return its length: 4.

  Your algorithm should run in O(n) complexity.
"""

class Solution:
    # @param A : tuple of integers
    # @return an integer
    def longestConsecutive1(self, A):
        if len(A) < 1: return 0
        if len(A) == 1: return 1

        s = set(A)
        maxres = 0

        for element in s:
            res = 1
            el = element
            while el+1 in s:
                res += 1
                el += 1
            maxres = max(res, maxres)
        return maxres

    def longestConsecutive2(self, A):
        pass

A = [ 97, 86, 67, 19, 107, 9, 8, 49, 23, 46, -4, 22, 72, 4, 57, 111, 20, 52, 99, 2, 113, 81, 7, 5, 21, 0, 47, 54, 76, 117, -2, 102, 34, 12, 103, 69, 36, 51, 105, -3, 33, 91, 37, 11, 48, 106, 109, 45, 58, 77, 104, 60, 75, 90, 3, 62, 16, 119, 85, 63, 87, 43, 74, 13, 95, 94, 56, 28, 55, 66, 92, 79, 27, 42, 70 ]

s = Solution()
print(s.longestConsecutive1(A))
print(s.longestConsecutive2(A))
