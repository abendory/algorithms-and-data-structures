def allFactors(A):
    factors = [1]
    if A == 1:
        return factors

    doublefactors = []
    for i in range(2, int(A**0.5)+1):
        if A % i == 0:
            factors.append(i)
            if i != int(A**0.5+1):
                doublefactors.append(A//i)
    if factors[-1] ** 2 == A and doublefactors[-1] ** 2 == A:
        factors.pop()
    print(factors, doublefactors)
    factors.extend(reversed(doublefactors))
    factors.append(A)
    return factors

print(allFactors(100))
