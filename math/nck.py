#from operator import mul    # or mul=lambda x,y:x*y
from fractions import Fraction
from functools import reduce

def nCk(n,k): 
    mul = lambda x, y: x*y
    return int(reduce(mul, (Fraction(n-i, i+1) for i in range(k)), 1))

#Test - printing Pascal's triangle:

for n in range(16):
    print(' '.join('%5d' % nCk(n,k) for k in range(n+1)).center(95))

"""
e.g., 6c4


6   5   4   3
- * - * - * -
1   2   3   4
"""
