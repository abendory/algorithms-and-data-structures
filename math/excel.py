"""
 Convert an excel column in to a number
  e.g., A => 1, Z => 26, AA => 27, AZ => 52
"""
def titleToNumber(A):
    if not A or not len(A):
        return -1

    n = len(A)-1
    res, power = 0, 0
    for i in reversed(range(n+1)):
        res += 26**power * (ord(A[i])-ord('A')+1)
        power += 1

    return res

print(titleToNumber("AZ"))
