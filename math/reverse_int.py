class Solution:
    # @param A : integer
    # @return an integer
    def reverse(self, A):

        print(A)

        # negative number test
        neg = False
        if A < 0:
            A *= -1
            neg = True

        res = 0
        while A != 0:
            res = res * 10 + A % 10
            A //= 10

        # overflow protection
        if res > 2**31:
            return 0

        # negative number test
        if neg:
            res *= -1

        print(res)
        return res

s = Solution()
s.reverse(115307243)
