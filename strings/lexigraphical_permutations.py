# given string S, find the position of a given permutation P in an
# array of all the permutations ordered in lexigraphical order
# if P = 'table', for 't' you can skip all the permutations that
# start with a,b,l,e (4!)
# so jump 4! then repeat for 'a' and sum them all up
# in this example solution is 1 + 4*4! + 0*3! + 0*2! + 1*1!
