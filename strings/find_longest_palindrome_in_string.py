def expand_palindrome(string, left, right):
    """ expand palindrome for even/odd indices """

    if left < 0 or right > len(string)-1 or string[left].lower() != string[right].lower():
        return left+1, right-1

    return expand_palindrome(string, left-1, right+1)

def find_longest_palindrome(string):
    max_difference = final_left = final_right = 0
    for index in range(len(string)):
        left_odd, right_odd = expand_palindrome(string, index, index)
        left_even, right_even = expand_palindrome(string, index, index+1)
        if right_odd-left_odd > max_difference:
            max_difference, final_left, final_right = right_odd-left_odd, left_odd, right_odd
        if right_even-left_even > max_difference:
            max_difference, final_left, final_right = right_even-left_even, left_even, right_even

    return string[final_left:final_right+1]


string = "thisisalongstringtobeusedwiththefunctionabove\
withanoddelevenletterpalindromeappsppaandaneventwelve\
letterpalindromebananaananab"

string2 = "AbleWasIEreISawElba"

print(find_longest_palindrome(string))
print(find_longest_palindrome(string2))
