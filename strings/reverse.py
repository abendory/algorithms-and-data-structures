def reverse_string(array, start, end):
    while start < end:
        array[start], array[end] = array[end], array[start]
        start += 1
        end -= 1
    return array


def reverse(array):
    if len(array) <= 1:
        return array

    start = 0;
    end = len(array) - 1;

    reversed_string = reverse_string(array, start, end)

    for end in range(len(reversed_string)+1):
        if end == len(reversed_string) or reversed_string[end] == ' ':
            reversed_words = reverse_string(reversed_string, start, end-1)
            #print(''.join(reversed_words), start, end)
            start = end + 1

    return ''.join(reversed_words)


array = list('this is a string to reverse')
print(reverse(array))
