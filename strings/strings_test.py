""" Strings TEST """

""" Number 1 """

def is_palindrome(string):
    start, end = 0, len(string)-1
    while end > start:
        if string[start] != string[end]:
            return False
        start += 1
        end -= 1
    return True

def reverse_array(array, start, end):
    while start < end:
        array[start], array[end] = array[end], array[start]
        start += 1
        end -= 1
    return array

def rotate_left(array, d):
    n = len(array)
    array = reverse_array(array, 0, d-1)  # reverse first half
    array = reverse_array(array, d, n-1)  # reverse second half
    array = reverse_array(array, 0, n-1)  # reverse all
    return array

def is_rotation_palindrome(string):
    if is_palindrome(string):
        return True

    n = len(string)
    for i in range(n):
        if is_palindrome(string[i+1:n] + string[:i+1]):
            return True
    
    return False

def is_rotation_palindrome2(string):
    if is_palindrome(string):
        return True

    n = len(string)
    string = string+string
    for i in range(n):
        if is_palindrome(string[i:n+i]):
            return True

    return False

print(is_rotation_palindrome("amanaplanacanalpanama"))
print(is_rotation_palindrome("acanalpanamaamanaplan"))
print(is_rotation_palindrome("manaplanacanalpanamaa"))
print(is_rotation_palindrome("manaplanacanalpanamax"))

print(is_rotation_palindrome2("amanaplanacanalpanama"))
print(is_rotation_palindrome2("acanalpanamaamanaplan"))
print(is_rotation_palindrome2("manaplanacanalpanamaa"))
print(is_rotation_palindrome2("manaplanacanalpanamax"))

""" Number 2 """

def is_prime(n):
    if n < 2: return False
    if n % 2 == 0: return n == 2
    if n % 3 == 0: return n == 3
    if n % 5 == 0: return n == 5

    # only need to go to sqrt(n) and skip odds
    i = 7
    while i * i <= n:
        if n % i == 0:
            return False
        i += 2

    return True

for i in range(1000):
    if is_prime(i): print(i, end=' ')
print()


""" Number 3 """
import random
def readK(output, N):
    return random.randint(10, 100)

def readN(output, N):
    outputk = []
    total_bytes_read = 0
    while total_bytes_read <= N:
        bytes_read = readK(outputk, N)
        output.append(outputk)
        total_bytes_read += bytes_read

    return total_bytes_read

#print(readN([], 1000))

