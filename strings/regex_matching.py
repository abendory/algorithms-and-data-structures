"""
 Match a regular expression:
  dot '.' match any single char
  star '*' match zero or more of the preceding chars
  carat '^' match beginning of string
  dollar '$' match end of string
"""

def regex(pattern, string):
    def regex(pattern, i, string, j):
        if i == len(pattern):
            return j == len(string)

        # start of string
        if pattern[i] == '^' and j == 0:
            return regex(pattern, i+1, string, j)

        # wildcard char
        # consume zero OR if allowed, consume one
        if i+1 < len(pattern) and pattern[i+1] == '*':
            return regex(pattern, i+2, string, j) or \
                (j != len(string) and pattern[i] == string[j] or \
                 pattern[i] == '.' and regex(pattern, i, string, j+1))

        # any character
        if j != len(string) and (pattern[i] == '.' or pattern[i] == string[j]):
            return regex(pattern, i+1, string, j+1)

        # end of string
        if pattern[i] == '$' and j == len(string):
            return regex(pattern, i+1, string, j)

        return False

    if pattern == None or string == None: return False
    return regex(pattern, 0, string, 0)

print(regex("^hat*", "hatx"))  # true
print(regex("^x*hat$", "xhat")) # true
print(regex("^hat$", "hat")) # true
print(regex("^hat", "^hat")) # false
print(regex("hat$", "hatx")) # false
print(regex("^xhatx*$", "xhatxxxx")) # true

