class Solution:
    # @param A : string
    # @return an integer
    def isPalindrome(self, A):
        if len(A) <= 1:
            return True
        
        start = 0
        end = len(A)-1
        
        while end > start:

            while A[start] == ' ' or ord(A[start]) < 48 or (ord(A[start]) > 57 and ord(A[start]) < 65) or (ord(A[start]) > 90 and ord(A[start]) < 97) or ord(A[start]) > 122:
                start += 1
                if start > len(A)-1: break

            while A[end] == ' ' or ord(A[end]) < 48 or (ord(A[end]) > 57 and ord(A[end]) < 65) or (ord(A[end]) > 90 and ord(A[end]) < 97) or ord(A[end]) > 122:
                end -= 1
                if end < 0: break

            if start < len(A) and end < len(A):
                if A[start].lower() != A[end].lower():
                    return False

            start += 1
            end -= 1
                
        return True

A = "A man, a plan, a canal: Panama"
A = "1a2"
s = Solution()
print(s.isPalindrome(A))
