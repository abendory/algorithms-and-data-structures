def spiral(string):
    def spiral(string, rows, cols):
        row, col = 0, 0
        while row < rows and col < cols:

            # print first row
            for i in range(col, cols):
                print(string[row][i], end = ' ')
            row += 1

            # print last col
            for i in range(row, rows):
                print(string[i][cols-1], end = ' ')
            cols -= 1

            # print last row
            if row < rows:
                for i in range(cols-1, col-1, -1):
                    print(string[rows-1][i], end = ' ')
                rows -= 1

            # print first col
            if col < cols:
                for i in range(rows-1, row-1, -1):
                    print(string[i][col], end=' ')
                col += 1

    spiral(string, len(string), len(string[0]))



string = [[1,  2,  3,  4,  5,  6],
          [7,  8,  9,  10, 11, 12],
          [13, 14, 15, 16, 17, 18]]

spiral(string)
