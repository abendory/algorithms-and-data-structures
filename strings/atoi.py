class Solution:
    # @param A : string
    # @return an integer
     def atoi(self, string):
        index, neg = 0, False
        n = len(string)

        # skip opening spaces
        while index < n and string[index] == ' ':
            index += 1

        # we're at the end
        if index == n:
            return 0

        # we're negative
        if string[index] == '-':
            neg = True
            index += 1

        # we're positive
        elif string[index] == '+':
            neg = False
            index += 1

        solution = 0
        for i in range(index, n):
            # we quit if we reach a nondigit value
            if not string[i].isdigit():
                break

            # otherwise we process that digit
            else:
                solution *= 10
                solution += int(string[i])
                
        # int limits
        if not neg and solution > 2147483647:
            return 2147483647
        elif neg and solution > 2147483648:
            return -2147483648
            
        if neg:
            return -1 * solution
        else:
            return solution
