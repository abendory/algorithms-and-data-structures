
def get_all_interleaving_strings(string1, string2, index, interleaved, result):
    m = len(string1)
    n = len(string2)
    if result == None: result = []

    if m == 0 and n == 0:
        result.append("".join(interleaved))

    if m != 0:
        interleaved[index] = string1[0]
        get_all_interleaving_strings(string1[1:], string2, index+1, interleaved, result)

    if n != 0:
        interleaved[index] = string2[0]
        get_all_interleaving_strings(string1, string2[1:], index+1, interleaved, result)

    return result

def is_interleaved(string3, string1, string2):
    res = get_all_interleaving_strings(string1, string2, 0, [None] * (len(string1)+len(string2)), result=None)
    return True if string3 in res else False

print(is_interleaved("1234", "123", "123"))  # False
print(is_interleaved("112233", "123", "123"))  # True
print(is_interleaved("123456", "123456", ""))  # True
print(is_interleaved("123456", "", "123456"))  # True
print(is_interleaved("12345678", "1234", "5678"))  # True
print(is_interleaved("12345678", "1233", "5678"))  # False
