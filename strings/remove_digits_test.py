

def remove_digits(string, number):
    n = len(string)
    res = []
    left = 0

    for j in range(number):
        min_digit = float('inf')
        for i in range(left, n-number+j):
            if string[i] < min_digit:
                min_digit = string[i]
                left = i
        res.append(min_digit)

    return ''.join(res)




