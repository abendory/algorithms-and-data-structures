
def insert(string):
    count = len(string)
    for char in string:
        if char == 'a':
            count += 1
        elif char == 'b':
            count -= 1
    if count < 1: count = 0
    new_string = [None] * count
    print(new_string)

    s_index = 0
    ns_index = 0
    while s_index < len(string):
        if string[s_index] == 'a':
            new_string[ns_index] = 'z'
            new_string[ns_index+1] = 'z'
            ns_index += 2
        elif string[s_index] == 'b':
            pass
        else:
            new_string[ns_index] = string[s_index]
            ns_index += 1
        s_index += 1

    return ''.join(new_string)

string = "habitat"
print(string)
print(insert(string))

