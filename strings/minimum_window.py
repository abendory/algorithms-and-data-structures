"""
1) Build a count array count[] for string 2. The count array stores counts of characters.
count[‘i’] = 1
count[‘t’] = 2
count[‘s’] = 1

2) Scan the string1 from left to right until we find all the characters of string2. To check if all the characters are there, use count[] built in step 1. So we have substring “this is a t” containing all characters of string2. Note that the first and last characters of the substring must be present in string2. Store the length of this substring as min_len.

3) Now move forward in string1 and keep adding characters to the substring “this is a t”. Whenever a character is added, check if the added character matches the left most character of substring. If matches, then add the new character to the right side of substring and remove the leftmost character and all other extra characters after left most character. After removing the extra characters, get the length of this substring and compare with min_len and update min_len accordingly.

Basically we add ‘e’ to the substring “this is a t”, then add ‘s’ and then’t’. ‘t’ matches the left most character, so remove ‘t’ and ‘h’ from the left side of the substring. So our current substring becomes “is a test”. Compare length of it with min_len and update min_len.
Again add characters to current substring “is a test”. So our string becomes “is a test str”. When we add ‘i’, we remove leftmost extra characters, so current substring becomes “t stri”. Again, compare length of it with min_len and update min_len. Finally add ‘n’ and ‘g’. Adding these characters doesn’t decrease min_len, so the smallest window remains “t stri”.

4) Return min_len.
Algorithm:

1. Find the first substring that contains all characters.
2. Try to minimize this substring by removing extraneous characters from the start of the substring while ensuring that the resulting substring still has all the characters in str2.
3. Save the position of startChar and endChar for this substring.
4. Try to find shorter windows:
Keep traversing the string str1 further till you find a character is equal to str1[startChar].
Now you can try to remove extraneous characters from the start of the old substring to create a new substring while ensuring that the resulting substring has all the characters in str2.
If this substring is smaller than the previously saved substring, update startChar and endChar.
"""

import sys

def minimum_window(char_set, string, left=0, right=0):
    left = right = min_length = 0
    m = len(char_set)

    """
     1) Build a count array count[] for char_set.
    """
    count = [0] * 256
    for char in char_set:
        count[ord(char)] += 1

    """
     2) Scan the string1 from left to right until we find all the characters of string2. Store the length of this substring as min_len.
    """
    while m:
        for index, char in enumerate(string):
            if char in char_set:
                count[ord(char)] -= 1
                if count[ord(char)] == 0:
                    m -= 1
            if m == 0:
                right = index
                break
    min_len = right - left
    print(index, min_len)

    """
     3) Now move forward in string1 and keep adding characters to the substring “this is a t”. Whenever a character is added, check if the added character matches the left most character of substring. If matches, then add the new character to the right side of substring and remove the leftmost character and all other extra characters after left most character. After removing the extra characters, get the length of this substring and compare with min_len and update min_len accordingly.
    """



    return min_len

string = "deadefgbpoiuucebsdasdfcd"
char_set = ['a','b','c']

l, r = minimum_window(char_set, string)
print(string[l:r])  # 'bsdasdfc'
