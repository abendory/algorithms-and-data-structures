def printSnakeString(string):
    print('  ', end='')
    for i in range(2, len(string), 4):
        if string[i] == ' ':
            print('~', end='   ')
        else:
            print(string[i], end='   ')
    print()
    print(' ', end='')
    for i in range(1, len(string), 2):
        if string[i] == ' ':
            print('~', end=' ')
        else:
            print(string[i], end=' ')
    print()
    for i in range(0, len(string), 4):
        if string[i] == ' ':
            print('~', end='   ')
        else:
            print(string[i], end='   ')

printSnakeString("letstryareallylongstring")
