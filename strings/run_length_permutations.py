"""
 Run Length Encoding ++:
  Generate all permutations for a given string where
  chars between first and last are replaced with numbers
  e.g., 'encode' = e4e, e3de, en3e, e2ode, en2de, enc2e, ..., enc 1 de, enco 1 e, encode
"""


def run_length_encoding_plus(st):
    print(st[0], len(st)-2, st[-1])
    for i in range(len(st)-3, 0, -1):
        for k in range(len(st)-1-i):
            print(i, k)
            print(st[:k+1], i, st[i+k+1:])
    print(st)

print(run_length_encoding_plus("encode"))


