#!/usr/bin/env python3

# Given a short pattern and long text, determine whether the pattern appears in the text

"""algorithm kmp_search:
    input:
        an array of characters, S (the text to be searched)
        an array of characters, W (the word sought)
    output:
        an integer (the zero-based position in S at which W is found)

    define variables:
        an integer, m ← 0 (the beginning of the current match in S)
        an integer, i ← 0 (the position of the current character in W)
        an array of integers, T (the table, computed elsewhere)

    while m + i < length(S) do
        if W[i] = S[m + i] then
            if i = length(W) - 1 then
                return m
            let i ← i + 1
        else
            if T[i] > -1 then
                let m ← m + i - T[i], i ← T[i]
            else
                let i ← 0, m ← m + 1

    (if we reach here, we have searched all of S unsuccessfully)
    return the length of S
"""

def kmp(text, pattern):
    '''
     Yields all starting positions of copies of the pattern in the text.
      Calling conventions are similar to string.find, but its arguments can be
      lists or iterators, not just strings, it returns all matches, not just
      the first one, and it does not need the whole text in memory at once.
      Whenever it yields, it will have read the text exactly up to and including
      the match that caused the yield.
    '''

    # build table of shift amounts
    shifts = [1] * (len(pattern) + 1)
    shift = 1
    for pos in range(len(pattern)):
        while shift <= pos and pattern[pos] != pattern[pos-shift]:
            shift += shifts[pos-shift]
        shifts[pos+1] = shift

    print(shifts)

    # do the actual search
    startPos = 0
    matchLen = 0
    for c in text:
        while matchLen == len(pattern) or \
              matchLen >= 0 and pattern[matchLen] != c:
            startPos += shifts[matchLen]
            matchLen -= shifts[matchLen]
        matchLen += 1
        if matchLen == len(pattern):
            return startPos


print(kmp("thisisalongersentencewithabunchofwordsinittobesearchedbythekmpalgorithm", "kmp"))
