def findWords(word_list, board):
    pass



class TrieNode:
    def __init__(self, word=None, nodes=None):
        pass

    def __repr__(self):
        return str(self.word)

class Trie:
    def __init__(self):
        self.root = TrieNode()

    def insert(self, val):
        pass

    def search(self, val):
        pass


def populate_trie(word_list):
    words = Trie()
    for word in word_list:
        words.insert(word)

    return words


word_list = ['GEEKS', 'FOR', 'QUIZ', 'GO']

board = [['G','I','Z'],
         ['U','E','K'],
         ['Q','S','E']]

trie_list = populate_trie(word_list)

for word in findWords(trie_list, board):
    print(word.lower())
