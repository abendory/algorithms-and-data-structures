def dfs(graph, start):
    stack, visited = [start], []
    while stack:
        vertex = stack.pop()
        if vertex not in visited:
            visited.add(vertex)
            stack.extend(graph[vertex]-visited)
    return visited


def bfs(graph, start):
    queue, visited = [start], []
    while queue:
        vertex = queue.pop(0)
        if vertex not in visited:
            visited.add(vertex)
            queue.extend(graph[vertex]-visited)
    return visited


def dfs_recursive(graph, start, visited=None):
    if not visited:
        visited = []
    visited.add(start)
    for vertex in graph[start]-visited:
        dfs_recursive(graph, vertex, visited)
    return visited

graph = { 'A': ['B', 'C'],
          'B': ['A', 'D', 'E'],
          'C': ['A', 'F'],
          'D': ['B'],
          'E': ['B', 'F'],
          'F': ['C', 'E']}


dfs(graph, 'A')
dfs_recursive(graph, 'A')
