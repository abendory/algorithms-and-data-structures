from structs import Graph, Vertex

"""
 Algorithm
 shortest_path(graph, weights, start)
 init (g, s)
 d[s] <- 0
 starting_set S <- None
 vertices Q <- all vertices V[G]
 verices will move from Q to S
 while Q is not empty:
    u <- extract min from Q and remove from Q
    S <- S Union{u}
    for each vertex v touching u:
        relax (u,v,w)
"""
class Dijkstra:
    """ Dijkstra’s algorithm for the shortest path problem. T[O(ElogV)]"""
    def __init__(self, graph):
        if not _directed():
            raise ValueError("graph is not directed")
        self.graph = graph
        self.distance = dict((node, float("inf")) for node in self.graph.iternodes())
        self.parent =   dict((node,None) for node in self.graph.iternodes())
        self.in_queue = dict((node,True) for node in self.graph.iternodes())
        self.pq = PriorityQueue()

    def run(self, source):
        self.source = source
        self.distance[source] = 0
        for node in self.graph.iternodes():
            self.pq.put((self.distance[node],node))
        while not self.pq.empty():
            _, node = self.pq.get()
            if self.in_queue[node]:
                self.in_queue[node] = False
            else:
                continue
            for edge in self.graph.iteroutedges(node):
                if self.in_queue[edge.target] and self._relax(edge):
                    self.pq.put((self.distance[edge.target], edge.target))


# DAG
dag = { 'A': {'B':5,  'C':3},
        'B': {'D':6,  'C':2},
        'C': {'E':4,  'F':2},
        'D': {'E':-1, 'F':7},
        'E': {'F':-2       }}

# DG
dg = { 'A': {'B':10, 'C':3       },
       'B': {'C':1,  'D':1       },
       'C': {'B':4,  'D':8, 'E':2},
       'D': {'E':7               },
       'E': {'D':9               }}

dj = Dijkstra(dg)
