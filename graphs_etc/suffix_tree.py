class SuffixNode:
    def __init__(self, data=None, children=None, is_terminal=False):
        self.data = data
        self.children = {}
        self.is_terminal = is_terminal

class SuffixTree:
    def __init__(self):
        self.root = SuffixNode()

    def insert(self, word):
        def insert(word, node, is_terminal=False, inserted=False):
            if is_terminal:
                terminal_node = SuffixNode('$')
                node.children[word[0]] = terminal_node
                return
            if word[0] not in node.children:
                new_node = SuffixNode(word[0])
                node.children[word[0]] = new_node
                insert(word, node)
            else:
                next_node = node.children[word[0]]
                if len(word[1:]) == 0:
                    return insert(word[1:], next_node, is_terminal=True)
                return insert(word[1:], next_node)
        hashmap = {}
        while word != '':
            if word not in hashmap:
                hashmap[word] = 1
                insert(word, self.root, inserted=True)
            else:
                hashmap[word] += 1
                insert(word, self.root)
            word = word[1:]

    def search(self, word):
        def search(word, node):
            if word[0] not in node.children:
                return False
            else:
                next_node = node.children[word[0]]
                if len(word[1:]) == 0:
                    return node.is_terminal
                return search(word[1:], next_node)
        if len(word) == 0:
            return False
        search(word, self.root)


st1 = SuffixTree()
st1.insert("AAAAAAAAAA")
print(st1.root.children)

st2 = SuffixTree()
st2.insert("ABCDEFG")
print(st2.root.children)

st3 = SuffixTree()
st3.insert('ABABABA')
print(st3.root.children)

st4 = SuffixTree()
st4.insert("ATCGA")
print(st4.root.children)

st5 = SuffixTree()
st5.insert('banana')
print(st5.root.children)

st6 = SuffixTree()
st6.insert('abcpqrabpqpq')
print(st6.root.children)

st7 = SuffixTree()
st7.insert('pqrpqpqabab')
print(st7.root.children)

