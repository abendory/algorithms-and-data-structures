class TrieNode:
    def __init__(self, word=None, nodes=None, is_terminal=False):
        self.word = word
        self.nodes = [None]*256
        self.is_terminal = is_terminal

    def __repr__(self):
        if self.word: return str(chr(self.word))
        return str("None")

    def __get_all__(self):
        """Get all of the words in the trie"""
        x = []
        for node in self.nodes:
            if(node is not None):
                x.append(str(node.word))
                x += node.__get_all__()
        return x

class Trie:
    def __init__(self):
        self.root = TrieNode()

    def get_all(self):
        return self.root.__get_all__()

    def insert(self, word):
        def insert(word, node):
            first_letter = ord(word[0])
            remaining_letters = word[1:]
            if node.nodes[first_letter] is None:
                new_node = TrieNode(first_letter)
                node.nodes[first_letter] = new_node
                insert(word, node)
            else:
                next_node = node.nodes[first_letter]
                if len(remaining_letters) == 0:
                    next_node.is_terminal = True
                    return
                next_node.is_terminal = False
                return insert(remaining_letters, next_node)
        insert(word, self.root)
        return

    def search(self, word):
        def search(word, node):
            first_letter = word[0]
            remaining_letters = word[1:]
            if node.nodes[first_letter] is None:
                return False
            else:
                next_node = node.nodes[first_letter]
                if len(remaining_letters) == 0:
                    return node.is_terminal
                return search(remaining_letters, next_node)
        if len(word) == 0:
            return False
        return search(word, self.root)


UP, UP_LEFT, UP_RIGHT, DOWN, DOWN_LEFT, DOWN_RIGHT, LEFT, RIGHT = \
     (0, -1), (-1, -1), (1, -1), (0, 1), (-1, 1), (1, 1), (-1, 0), (1, 0)
DIRECTIONS = (UP, UP_LEFT, UP_RIGHT, DOWN, DOWN_LEFT, DOWN_RIGHT, LEFT, RIGHT)

def findWords(word_list, board):

    rows = len(board)
    cols = len(board[0])

    stack = []

    for y in range(cols):
        for x in range(rows):
            char = board[y][x]
            node = word_list.root.nodes[ord(char)]
            if node is not None:
                stack.append((x, y, char, node))

    while stack:
        # get current coordinates, the character from board, and the node from the dict
        x, y, string, node = stack.pop()

        # check every direction
        for dx, dy in (DIRECTIONS):

            # calculate the next coordinates by appending the direction
            # and make sure we're in bounds
            next_x, next_y = x + dx, y + dy
            if 0 <= next_x < cols and 0 <= next_y < rows:
                next_string = string + board[next_y][next_x]
                new_node = node.nodes[ord(board[next_y][next_x])]
                if new_node is not None:
                    if new_node.is_terminal:
                        yield next_string
                    stack.append((next_x, next_y, next_string, new_node))

def populate_trie(is_short_list=False):
    words = Trie()
    if is_short_list:
        with open('/usr/share/dict/propernames', 'r') as f:
            word = f.readline()
            while word:
                words.insert(word.strip().upper())
                word = f.readline()
    else:
        word_list = ['GEEKS', 'FOR', 'QUIZ', 'GO']
        for word in word_list:
            words.insert(word)

    return words

board = [['S','E','Z','I'],
         ['P','T','A','N'],
         ['Q','H','E','T'],
         ['X','R','U','C']]

short_list = populate_trie(is_short_list=False)
long_list = populate_trie(is_short_list=True)

for word in findWords(long_list, board):
    print(word.lower())
