from bitarray import bitarray
import mmh3   # hash

class BloomFilter:
    def __init__(self, size, number_of_hashes):
        self.size = size
        self.number_of_hashes = number_of_hashes
        self.bit_array = bitarray(size)
        self.bit_array.setall(0)

    def add(self, value):
        for count in range(self.number_of_hashes):
            key = mmh3.hash(value, count) % self.size
            self.bit_array[key] = 1

    def lookup(self, value):
        #print(self.bit_array)
        for count in range(self.number_of_hashes):
            key = mmh3.hash(value, count) % self.size
            if self.bit_array[key] == 0:
                print("String not found")
                return
        print("String hit")

# dict with 235886 words
# bloom filter of 3 million bits with 7 hashes
# chance of false positive is (1-(1-1/3000000)^(7*235886))^7 = 0.24%
bf = BloomFilter(3000000, 7)
with open('/usr/share/dict/words', 'r') as f:
    words = f.read().splitlines()
    for word in words:
        bf.add(word.strip())

bf.lookup("hello")
bf.lookup("starwars")
