#!/usr/bin/env python3

class TrieNode:
    """ Node storing character and array of children
          nodes and is_terminal boolean """
    def __init__(self, char=None, children=None, is_terminal=False):
        self.char = char
        self.children = {}
        self.is_terminal = is_terminal

class Trie:
    def __init__(self):
        self.root = TrieNode()

    def insert(self, word):
        def insert(word, node):
            first_letter = word[0]
            remaining_letters = word[1:]
            if first_letter not in node.children:
                new_node = TrieNode(first_letter)
                node.children[first_letter] = new_node
                insert(word, node)
            else:
                next_node = node.children[first_letter]
                if len(remaining_letters) == 0:
                    next_node.is_terminal = True
                    return
                node.is_terminal = False
                return insert(remaining_letters, next_node)
        insert(word, self.root)
        return

    def search(self, word):
        def search(word, node):
            first_letter = word[0]
            remaining_letters = word[1:]
            if first_letter not in node.children:
                return False
            else:
                next_node = node.children[first_letter]
                if len(remaining_letters) == 0:
                    return node.is_terminal
                return search(remaining_letters, next_node)
        if len(word) == 0:
            return False
        return search(word, self.root)

    def starts_with(self, word):
        def starts_with(word, node):
            first_letter = word[0]
            remaining_letters = word[1:]
            if first_letter not in node.children:
                return False
            else:
                if len(remaining_letters) == 0:
                    return True
                next_node = node.children[first_letter]
                return starts_with(remaining_letters, next_node)
        if len(word) == 0:
            return False
        return starts_with(word, self.root)


# Populate the Trie with first names
name_trie = Trie()
with open('/usr/share/dict/propernames', 'r') as f:
    name = f.readline().strip()
    while name:
        name_trie.insert(name)
        name = f.readline().strip()

print(name_trie.search("David"))
print(name_trie.search("Addie"))
name_trie.insert("Addie")
print(name_trie.search("Addie"))
print(name_trie.search("DarthVader"))
print(name_trie.starts_with("Add"))
print(name_trie.starts_with("Da"))
print(name_trie.starts_with("234"))
