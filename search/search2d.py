#!/usr/bin/env Python

def search2d(m, t):
    """ return True/False if element is present """
    rows = len(m)
    cols = len(m[0])
    start_row = 0
    start_col = 0
    end_row = rows-1
    end_col = cols-1

    while start_row <= end_row:
        mid_row = start_row + (end_row - start_row) // 2
        if t == m[mid_row][start_col] or t == m[mid_row][end_col]:
            return True
        if t > m[mid_row][start_col] and t < m[mid_row][end_col]:
            return search1d(m[mid_row], t)
        elif t < m[mid_row][start_col]:
            end_row = mid_row - 1
        elif t > m[mid_row][end_col]:
            start_row = mid_row + 1

    return False

def search1d(m, t):
    start = 0
    end = len(m) - 1

    while start <= end:
        mid = start + (end - start) // 2
        if t == m[mid]:
            return True
        elif t <= m[mid]:
            end = mid - 1
        elif t > m[mid]:
            start = mid +1

    return False


m = [
  [2, 3, 4, 6],
  [16, 19, 33, 36],
  [37, 38, 38, 41],
  [47, 47, 50, 51],
  [55, 57, 58, 62],
  [63, 65, 66, 66],
  [68, 70, 75, 77],
  [78, 84, 84, 86],
  [86, 87, 88, 92],
  [94, 95, 96, 97]
]
# Test
print(search2d(m, 81))
