# calculate the sqrt of an integer

def sqrt(n):
    if n == 0: return n

    lo = 0
    hi = n+1

    while hi - lo > 1:
        mid = lo + (hi - lo) // 2
        if mid*mid <= n:
            lo = mid
        else:
            hi = mid
    print(lo)
    return lo

sqrt(4)
