"""
Regex Matcher
Implement a regular expression matcher, supporting the following characters
- a dot ('.') ‘.’ Matches any single character.
- a star ('*') ‘*’ Matches zero or more of the preceding element.
- a caret ('^') ‘^’ Matches if following element is at beginning of string.
- a dollar ('$') ‘$’ Matches if precending element is at end of string.

"""

def regex_matcher(string, regex):
    def match(string, strpos, regex, regpos):
        # base case
        if regpos == len(regex):
            return strpos == len(string)

        # # beginning
        # if regex[regpos] == r'^' and strpos == 0:
        #     return match(string, strpos, regex, regpos+1)

        # wildcard
        if regpos + 1 < len(regex) and regex[regpos+1] == '*':
            return match(string, strpos, regex, regpos+2) or \
                    (strpos != len(string) and
                     (regex[regpos] == string[strpos] or regex[regpos] == '.') and
                     match(string, strpos+1, regex, regpos))

        # anychar
        if strpos != len(string) and (regex[regpos] == string[strpos] or regex[regpos] == r'.'):
            return match(string, strpos+1, regex, regpos+1)

        # # end
        # if regex[regpos] == r'$' and strpos == len(string)-1:
        #     return match(string, strpos, regex, regpos+1)

        return False

    return match(string, 0, regex, 0)



print(regex_matcher("yuval", "yuval"))                 # true
print(regex_matcher("yuva", "yuval"))              # false
print(regex_matcher("yuval", "yuv"))                # false),
print(regex_matcher("yuval", "") )             # false),
print(regex_matcher("", "yuval"))                  # false),
print(regex_matcher("yuval", ".....") )                # true),
print(regex_matcher("yuval", "yuvb*al"))               # true),
print(regex_matcher("yuval", ".*") )                   # true),
print(regex_matcher("yuval", "a*b*c*yuval.*a*b*c*"))  # true),
print(regex_matcher("aaaabc","a*abc"))               # true)


